local i4, i2, i1, u4, u2, u1, call = mem.i4, mem.i2, mem.i1, mem.u4, mem.u2, mem.u1, mem.call

local function CreateButton(unk, action, actionInfo1, actionInfo2)
	local btn = mem.new(0xE0)
	return call(0x4C2439, 1, btn, unk, action, actionInfo1, actionInfo2)
end

local xy = mem.malloc(8)

local function SetupButton(btn, x, y, normalPic, pressedPic, hoverPic, disabledPic, transp, englishD)
	i4[xy] = x
	i4[xy + 4] = y
	return call(u4[u4[btn] + 168], 1, btn, xy, transp or 0, normalPic or 0, pressedPic or 0, hoverPic or 0, disabledPic or 0, englishD or 0)
end

local function ButtonHotKey(btn, key)
	return call(u4[u4[btn] + 68], 1, btn, key)
end

local function ButtonEnable(btn, on)
	return call(u4[u4[btn] + (on ~= false and 0x38 or 0x3C)], 1, btn)
end

local function DlgAddItem(dlg, item)
	return call(u4[u4[dlg] + 164], 1, dlg, item)
end

----- Variables -----

local PlayerNumbers = {0,
	1,
	7,
	48,
	49,
}

local PlayerDefaults = {
	{Face = 1, Stats = {20, 9, 9, 15, 15, 15, 9}, Skills = {4, 5}},
	{Face = 13, Stats = {15, 9, 15, 15, 15, 14, 9}, Skills = {9, 17}},
	{Face = 16, Stats = {15, 15, 9, 7, 29, 15, 9}, Skills = {1, 13}},
	{Face = 7, Stats = {11, 9, 20, 16, 16, 11, 9}, Skills = {16, 17}},
	{Face = 8, Stats = {9, 25, 9, 20, 9, 11, 9}, Skills = {13, 14}},
}

local NewGameDialog
local PlayerButtons
local CurrentPlayer

----- PlayerButtons creation -----

local BtnFrames = {
	[0] = {"Xc_P", "Xc_Pdn", "Xc_Pht"},
	{"Xc_1", "Xc_1dn", "Xc_1ht", "Xc_1dn"},
	{"Xc_2", "Xc_2dn", "Xc_2ht", "Xc_2dn"},
	{"Xc_3", "Xc_3dn", "Xc_3ht", "Xc_3dn"},
	{"Xc_4", "Xc_4dn", "Xc_4ht", "Xc_4dn"},
	{"Xc_5", "Xc_5dn", "Xc_5ht", "Xc_5dn"},
}

local function SetupPlayerButton(i)
	return SetupButton(PlayerButtons[i], 10 + (i-1)*32, 440, unpack(BtnFrames[(i <= Party.Count) and i or 0]))
end

local function CreatePlayerButton(i)
	local btn = CreateButton(1, 67, i, 0)
	PlayerButtons[i] = btn
	SetupPlayerButton(i)
	ButtonHotKey(btn, 0x30 + i)
	DlgAddItem(NewGameDialog, btn)
end

local function UpdateDialog()
	call(0x4C6B92, 1, NewGameDialog)
	call(0x4C6B69, 1, NewGameDialog)
end

----- Players manipulation -----

local function SetupPlayer(n)
	local pl = Party.Players[0]
	local def = PlayerDefaults[n]
	pl.Face = def.Face
	pl.Voice = def.Face
	i4[pl["?ptr"] + 0x1BE8] = def.Face
	i4[pl["?ptr"] + 0x1BEC] = def.Face
	pl.Class = call(0x48F552, 1, pl)
	call(0x4903C0, 1, 0xB20E90, 0, 0)
	for i = 1, 7 do
		pl.Stats[i-1].Base = def.Stats[i]
	end
	pl.Skills[def.Skills[1]] = 1
	pl.Skills[def.Skills[2]] = 1
end

local PlayerBuffers = {mem.malloc(Party.PlayersArray[0]["?size"]),
	Party.PlayersArray[PlayerNumbers[2]]["?ptr"],
	Party.PlayersArray[PlayerNumbers[3]]["?ptr"],
	Party.PlayersArray[PlayerNumbers[4]]["?ptr"],
	Party.PlayersArray[PlayerNumbers[5]]["?ptr"],
}

local function StorePlayer(n)
	if not n then
		-- copy name for character
		local name = call(0x4CD2BF, 1, NewGameDialog)
		call(0x4D9E20, 0, Party.PlayersArray[0]["?ptr"] + 0xA8, name)
	end
	mem.copy(PlayerBuffers[n or CurrentPlayer], Party.PlayersArray[0]["?ptr"], Party.PlayersArray[0]["?size"])
end

local function UsePlayer(n, updateDlg)
	if n <= Party.Players.Count then
		mem.copy(Party.PlayersArray[0]["?ptr"], PlayerBuffers[n], Party.PlayersArray[0]["?size"])
	else
		-- add to party
		Party.Players.Count = n
		Party.PlayersIndexes[n - 1] = PlayerNumbers[n]
		-- init new player
		SetupPlayer(n)
		-- update buttons
		SetupPlayerButton(n)
		if n < 5 then
			CreatePlayerButton(n + 1)
		end
	end
	if updateDlg then
		ButtonEnable(PlayerButtons[CurrentPlayer])
		ButtonEnable(PlayerButtons[n], false)
	end
	CurrentPlayer = n
end

----- Add 2 character buttons to the dialog and do initial setup -----

mem.autohook(0x4C67C0, function(d)
	NewGameDialog = d.ebx
	-- add buttons
	PlayerButtons = {}
	CreatePlayerButton(1)
	CreatePlayerButton(2)
	-- current player
	CurrentPlayer = 1
	ButtonEnable(PlayerButtons[1], false)
	SetupPlayer(1)
	UpdateDialog()
end)

----- Character buttons (1 - 5) -----

mem.hook(0x433351, function(d)
	if not d.ZF then
		u4[d.esp] = 0x433AF8
	else
		local info1 = i4[d.esp + 0x24 - 0x10]
		if info1 > 0 then
			StorePlayer()
			UsePlayer(info1, true)
			UpdateDialog()
			u4[d.esp] = 0x43344F
		end
	end
end, 6)

----- OK button - Test all characters -----

mem.hook(0x433395, function(d)
	u4[d.esp] = 0x4333B6
	if call(0x48FA68, 0) ~= 0 or call(0x48FA3C, 0) == 0 then  -- if not finished
		return
	end
	StorePlayer()
	for i = 1, Party.Players.Count do
		UsePlayer(i, true)
		if call(0x48FA68, 0) ~= 0 or call(0x48FA3C, 0) == 0 then  -- if not finished
			return UpdateDialog()
		end
	end
	UpdateDialog()
	for i = 2, Party.Players.Count do  -- set Hired bits
		Party.QBits[400 + PlayerNumbers[i]] = true
	end
	u4[d.esp] = 0x4333A7
end)

----- Fill inventory for all players ------

mem.hook(0x49429C, function(d)
	if CurrentPlayer > 1 then
		d:pop()
		d:pop()
		u4[d.esp] = 0x494007
		-- do the same for previous player
		StorePlayer(CurrentPlayer)
		UsePlayer(CurrentPlayer - 1)		
		call(0x4C6B69, 1, NewGameDialog)
	else
		d.ecx = 0xFEB360
	end
end)
