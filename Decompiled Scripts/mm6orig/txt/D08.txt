str[0] = " "
str[1] = "Door"
str[2] = "Chest"
str[3] = "Poisoned Spike"
str[4] = "Unstable rock"
str[5] = "Suspicious Floor"
str[6] = "Dark pit"
str[7] = "Click!"
str[8] = "Exit"
str[9] = "Switch"
str[10] = "Trap!"
str[11] = "Ca-Click!"
str[12] = "Snick!"
str[13] = "Magic Gate Open"
str[14] = "Bag"
str[15] = "Spiral Stuff (Bug if this displays)."
str[16] = "Magic Door"
str[17] = "Bag"
str[18] = "You cannot see me, hear me or touch me.  I lie behind the stars and alter what is real, I am what you really fear, Close your eyes and come I near. What am I?"
str[19] = "dark"
str[20] = "darkness"
str[21] = "Answer?  "
str[22] = "Incorrect"
str[23] = "I go through an apple, or point out your way, I fit in a bow, then a target, to stay. What am I?"
str[24] = "arrow"
str[25] = "an arrow"
str[26] = "What consumes rocks, levels mountains, destroys wood, pushes the clouds across the sky, and can make a young one old?"
str[27] = "time"
str[28] = "Alive without breath, as cold as death, never thirsty ever drinking, all in mail never clinking, ever travelling, never walking, mouth ever moving, never talking.  What am I? "
str[29] = "fish"
str[30] = "a fish"
str[31] = "Magical Wall"
str[32] = "Sack"
str[33] = "Empty"
str[34] = "Shadow Guild"
str[35] = ""


event 1
      Hint = str[1]  -- "Door"
      MazeInfo = str[34]  -- "Shadow Guild"
  0:  SetDoorState  {Id = 1, State = 1}
  1:  SetDoorState  {Id = 2, State = 1}
end

event 2
  0:  OnLoadMap  {}
  1:  Set  {"MapVar0", Value = 0}
  2:  Set  {"MapVar1", Value = 0}
  3:  Set  {"MapVar2", Value = 0}
  4:  Set  {"MapVar3", Value = 0}
  5:  Set  {"MapVar4", Value = 0}
  6:  Set  {"MapVar5", Value = 0}
  7:  Set  {"MapVar6", Value = 0}
  8:  Set  {"MapVar7", Value = 0}
  9:  Set  {"MapVar8", Value = 0}
  10: Set  {"MapVar9", Value = 0}
  11: Set  {"MapVar10", Value = 0}
  12: Set  {"MapVar11", Value = 0}
  13: Set  {"MapVar12", Value = 0}
  14: Set  {"MapVar13", Value = 0}
  15: Set  {"MapVar14", Value = 0}
  16: Set  {"MapVar15", Value = 0}
  17: Set  {"MapVar16", Value = 0}
  18: SetDoorState  {Id = 3, State = 1}
  19: SetDoorState  {Id = 4, State = 0}
  20: SetDoorState  {Id = 8, State = 0}
  21: SetDoorState  {Id = 1, State = 0}
  22: SetDoorState  {Id = 2, State = 0}
  23: SetDoorState  {Id = 11, State = 0}
  24: SetDoorState  {Id = 9, State = 0}
  25: SetDoorState  {Id = 5, State = 0}
  26: SetDoorState  {Id = 61, State = 0}
  27: SetDoorState  {Id = 62, State = 0}
  28: SetDoorState  {Id = 63, State = 0}
  29: SetDoorState  {Id = 64, State = 0}
  30: Cmp  {"MapVar0", Value = 1,   jump = 32}
  31: GoTo  {jump = 33}

  32: SetTexture  {Facet = 833, Name = "T3S1ON"}
  33: Cmp  {"MapVar15", Value = 1,   jump = 35}
  34: Exit  {}

  35: SetTexture  {Facet = 1079, Name = "mystryB"}
end

event 3
      Hint = str[9]  -- "Switch"
  0:  SetDoorState  {Id = 3, State = 0}
  1:  SetDoorState  {Id = 4, State = 1}
end

event 5
  0:  SetDoorState  {Id = 5, State = 1}
end

event 6
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 6, State = 1}
end

event 7
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 7, State = 1}
end

event 8
      Hint = str[1]  -- "Door"
  0:  Cmp  {"MapVar0", Value = 1,   jump = 2}
  1:  DamagePlayer  {Player = "Current", DamageType = const.Damage.Fire, Damage = 15}
  2:  SetDoorState  {Id = 8, State = 1}
end

event 9
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 9, State = 1}
end

event 10
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 10, State = 1}
end

event 11
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 11, State = 1}
end

event 12
  0:  SetDoorState  {Id = 12, State = 1}
end

event 13
      Hint = str[5]  -- "Suspicious Floor"
  0:  Cmp  {"MapVar0", Value = 1,   jump = 4}
  1:  Set  {"MapVar0", Value = 1}
  2:  SetTexture  {Facet = 833, Name = "T3S1ON"}
  3:  Exit  {}

  4:  Set  {"MapVar0", Value = 0}
  5:  SetTexture  {Facet = 833, Name = "T3S1OFF"}
end

event 14
      Hint = str[5]  -- "Suspicious Floor"
  0:  Cmp  {"MapVar3", Value = 1,   jump = 6}
  1:  Set  {"MapVar1", Value = 1}
  2:  Cmp  {"MapVar2", Value = 1,   jump = 4}
  3:  Exit  {}

  4:  Set  {"MapVar3", Value = 1}
  5:  StatusText  {Str = 7}         -- "Click!"
  6:  Exit  {}
end

event 15
      Hint = str[5]  -- "Suspicious Floor"
  0:  Cmp  {"MapVar3", Value = 1,   jump = 6}
  1:  Set  {"MapVar2", Value = 1}
  2:  Cmp  {"MapVar1", Value = 1,   jump = 4}
  3:  Exit  {}

  4:  Set  {"MapVar3", Value = 1}
  5:  StatusText  {Str = 7}         -- "Click!"
  6:  Exit  {}
end

event 16
      Hint = str[5]  -- "Suspicious Floor"
  0:  Cmp  {"MapVar6", Value = 1,   jump = 6}
  1:  Set  {"MapVar4", Value = 1}
  2:  Cmp  {"MapVar5", Value = 1,   jump = 4}
  3:  Exit  {}

  4:  Set  {"MapVar6", Value = 1}
  5:  StatusText  {Str = 11}         -- "Ca-Click!"
  6:  Exit  {}
end

event 17
      Hint = str[5]  -- "Suspicious Floor"
  0:  Cmp  {"MapVar6", Value = 1,   jump = 6}
  1:  Set  {"MapVar5", Value = 1}
  2:  Cmp  {"MapVar4", Value = 1,   jump = 4}
  3:  Exit  {}

  4:  Set  {"MapVar6", Value = 1}
  5:  StatusText  {Str = 11}         -- "Ca-Click!"
  6:  Exit  {}
end

event 18
      Hint = str[5]  -- "Suspicious Floor"
  0:  Cmp  {"MapVar9", Value = 1,   jump = 6}
  1:  Set  {"MapVar7", Value = 1}
  2:  Cmp  {"MapVar8", Value = 1,   jump = 4}
  3:  Exit  {}

  4:  Set  {"MapVar9", Value = 1}
  5:  StatusText  {Str = 12}         -- "Snick!"
  6:  Exit  {}
end

event 19
      Hint = str[5]  -- "Suspicious Floor"
  0:  Cmp  {"MapVar9", Value = 1,   jump = 6}
  1:  Set  {"MapVar8", Value = 1}
  2:  Cmp  {"MapVar7", Value = 1,   jump = 4}
  3:  Exit  {}

  4:  Set  {"MapVar9", Value = 1}
  5:  StatusText  {Str = 12}         -- "Snick!"
  6:  Exit  {}
end

event 20
      Hint = str[5]  -- "Suspicious Floor"
  0:  Cmp  {"MapVar12", Value = 1,   jump = 6}
  1:  Set  {"MapVar10", Value = 1}
  2:  Cmp  {"MapVar11", Value = 1,   jump = 4}
  3:  Exit  {}

  4:  Set  {"MapVar12", Value = 1}
  5:  StatusText  {Str = 7}         -- "Click!"
  6:  Exit  {}
end

event 21
      Hint = str[5]  -- "Suspicious Floor"
  0:  Cmp  {"MapVar12", Value = 1,   jump = 6}
  1:  Set  {"MapVar11", Value = 1}
  2:  Cmp  {"MapVar10", Value = 1,   jump = 4}
  3:  Exit  {}

  4:  Set  {"MapVar12", Value = 1}
  5:  StatusText  {Str = 7}         -- "Click!"
  6:  Exit  {}
end

event 22
      Hint = str[5]  -- "Suspicious Floor"
  0:  Cmp  {"MapVar15", Value = 1,   jump = 8}
  1:  CastSpell  {Spell = 2, Mastery = const.Novice, Skill = 2, FromX = -400, FromY = 3072, FromZ = 190, ToX = -1664, ToY = 4416, ToZ = 120}         -- "Flame Arrow"
  2:  CastSpell  {Spell = 13, Mastery = const.Novice, Skill = 2, FromX = -8, FromY = 3456, FromZ = 120, ToX = -1664, ToY = 4416, ToZ = 120}         -- "Static Charge"
  3:  Set  {"MapVar13", Value = 1}
  4:  Cmp  {"MapVar14", Value = 1,   jump = 6}
  5:  Exit  {}

  6:  Set  {"MapVar15", Value = 1}
  7:  StatusText  {Str = 11}         -- "Ca-Click!"
  8:  Exit  {}
end

event 23
      Hint = str[5]  -- "Suspicious Floor"
  0:  Cmp  {"MapVar15", Value = 1,   jump = 8}
  1:  CastSpell  {Spell = 2, Mastery = const.Novice, Skill = 2, FromX = -1776, FromY = 3072, FromZ = 120, ToX = -384, ToY = 4416, ToZ = 120}         -- "Flame Arrow"
  2:  CastSpell  {Spell = 13, Mastery = const.Novice, Skill = 2, FromX = -2040, FromY = 3456, FromZ = 120, ToX = -384, ToY = 4416, ToZ = 120}         -- "Static Charge"
  3:  Set  {"MapVar14", Value = 1}
  4:  Cmp  {"MapVar13", Value = 1,   jump = 6}
  5:  Exit  {}

  6:  Set  {"MapVar15", Value = 1}
  7:  StatusText  {Str = 11}         -- "Ca-Click!"
  8:  Exit  {}
end

event 24
      Hint = str[5]  -- "Suspicious Floor"
  0:  Cmp  {"MapVar3", Value = 1,   jump = 2}
  1:  Exit  {}

  2:  Cmp  {"MapVar6", Value = 1,   jump = 4}
  3:  Exit  {}

  4:  Cmp  {"MapVar9", Value = 1,   jump = 6}
  5:  Exit  {}

  6:  Cmp  {"MapVar12", Value = 1,   jump = 8}
  7:  Exit  {}

  8:  Cmp  {"MapVar15", Value = 1,   jump = 10}
  9:  Exit  {}

  10: StatusText  {Str = 12}         -- "Snick!"
  11: FaceExpression  {Player = "All", Frame = 5}
  12: Set  {"MagicResBonus", Value = 15}
  13: Cmp  {"MapVar16", Value = 1,   jump = 19}
  14: StatusText  {Str = 13}         -- "Magic Gate Open"
  15: Set  {"MapVar16", Value = 1}
  16: SetTexture  {Facet = 1079, Name = "mystryB"}
  17: SummonMonsters  {TypeIndexInMapStats = 3, Level = 1, Count = 1, X = 2688, Y = 3968, Z = 1}
  18: MoveToMap  {X = 2179, Y = 4314, Z = 1, Direction = 1, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  19: Exit  {}
end

event 25
      Hint = str[5]  -- "Suspicious Floor"
  0:  StatusText  {Str = 10}         -- "Trap!"
  1:  CastSpell  {Spell = 15, Mastery = const.Novice, Skill = 3, FromX = -1024, FromY = 3840, FromZ = 120, ToX = -1024, ToY = 2816, ToZ = 120}         -- "Sparks"
  2:  CastSpell  {Spell = 15, Mastery = const.Novice, Skill = 3, FromX = -1024, FromY = 3840, FromZ = 120, ToX = -1024, ToY = 4864, ToZ = 120}         -- "Sparks"
  3:  CastSpell  {Spell = 15, Mastery = const.Novice, Skill = 3, FromX = -1024, FromY = 3840, FromZ = 120, ToX = 0, ToY = 3480, ToZ = 120}         -- "Sparks"
  4:  CastSpell  {Spell = 15, Mastery = const.Novice, Skill = 3, FromX = -1024, FromY = 3840, FromZ = 120, ToX = -2048, ToY = 3480, ToZ = 120}         -- "Sparks"
  5:  CastSpell  {Spell = 15, Mastery = const.Novice, Skill = 3, FromX = -1024, FromY = 3840, FromZ = 120, ToX = -256, ToY = 4608, ToZ = 120}         -- "Sparks"
  6:  CastSpell  {Spell = 15, Mastery = const.Novice, Skill = 3, FromX = -1024, FromY = 3840, FromZ = 120, ToX = -256, ToY = 3072, ToZ = 120}         -- "Sparks"
  7:  CastSpell  {Spell = 15, Mastery = const.Novice, Skill = 3, FromX = -1024, FromY = 3840, FromZ = 120, ToX = -1792, ToY = 3072, ToZ = 120}         -- "Sparks"
  8:  CastSpell  {Spell = 15, Mastery = const.Novice, Skill = 3, FromX = -1024, FromY = 3840, FromZ = 120, ToX = -1792, ToY = 4608, ToZ = 120}         -- "Sparks"
  9:  Set  {"MapVar3", Value = 0}
  10: Set  {"MapVar6", Value = 0}
  11: Set  {"MapVar9", Value = 0}
  12: Set  {"MapVar12", Value = 0}
  13: Set  {"MapVar15", Value = 0}
end

event 26
  0:  Cmp  {"MapVar16", Value = 1,   jump = 2}
  1:  Exit  {}

  2:  MoveToMap  {X = 8832, Y = -4992, Z = 87, Direction = 1, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 31
      Hint = str[14]  -- "Bag"
  0:  OpenChest  {Id = 1}
end

event 32
      Hint = str[3]  -- "Poisoned Spike"
  0:  DamagePlayer  {Player = "Random", DamageType = const.Damage.Phys, Damage = 16}
  1:  DamagePlayer  {Player = "Random", DamageType = const.Damage.Phys, Damage = 11}
end

event 34
      Hint = str[5]  -- "Suspicious Floor"
  0:  CastSpell  {Spell = 35, Mastery = const.Novice, Skill = 2, FromX = -1632, FromY = 2544, FromZ = 120, ToX = -1632, ToY = 768, ToZ = 120}         -- "Magic Arrow"
  1:  StatusText  {Str = 10}         -- "Trap!"
end

event 35
      Hint = str[5]  -- "Suspicious Floor"
  0:  CastSpell  {Spell = 35, Mastery = const.Novice, Skill = 3, FromX = 768, FromY = -1528, FromZ = 120, ToX = 768, ToY = -1280, ToZ = 120}         -- "Magic Arrow"
  1:  StatusText  {Str = 10}         -- "Trap!"
end

event 36
      Hint = str[4]  -- "Unstable rock"
  0:  CastSpell  {Spell = 41, Mastery = const.Novice, Skill = 8, FromX = 1728, FromY = 1152, FromZ = 240, ToX = 2176, ToY = 1152, ToZ = 32}         -- "Rock Blast"
  1:  StatusText  {Str = 10}         -- "Trap!"
end

event 39
      Hint = str[5]  -- "Suspicious Floor"
  0:  CastSpell  {Spell = 15, Mastery = const.Novice, Skill = 13, FromX = 2432, FromY = 3576, FromZ = 120, ToX = 2432, ToY = 2824, ToZ = 120}         -- "Sparks"
  1:  StatusText  {Str = 10}         -- "Trap!"
end

event 40
      Hint = str[5]  -- "Suspicious Floor"
  0:  CastSpell  {Spell = 15, Mastery = const.Expert, Skill = 10, FromX = 2056, FromY = 3200, FromZ = 120, ToX = 2800, ToY = 3200, ToZ = 120}         -- "Sparks"
  1:  StatusText  {Str = 10}         -- "Trap!"
end

event 41
      Hint = str[5]  -- "Suspicious Floor"
  0:  CastSpell  {Spell = 15, Mastery = const.Novice, Skill = 13, FromX = 2432, FromY = 2824, FromZ = 120, ToX = 2432, ToY = 3576, ToZ = 120}         -- "Sparks"
  1:  StatusText  {Str = 10}         -- "Trap!"
end

event 42
      Hint = str[5]  -- "Suspicious Floor"
  0:  CastSpell  {Spell = 15, Mastery = const.Expert, Skill = 10, FromX = 2800, FromY = 3200, FromZ = 120, ToX = 2056, ToY = 3200, ToZ = 120}         -- "Sparks"
  1:  StatusText  {Str = 10}         -- "Trap!"
end

event 43
  0:  OnTimer  {IntervalInHalfMinutes = 9}
  1:  Cmp  {"MapVar3", Value = 1,   jump = 4}
  2:  CastSpell  {Spell = 2, Mastery = const.Novice, Skill = 2, FromX = -1776, FromY = 3072, FromZ = 190, ToX = -400, ToY = 3072, ToZ = 120}         -- "Flame Arrow"
  3:  CastSpell  {Spell = 2, Mastery = const.Novice, Skill = 2, FromX = -400, FromY = 3072, FromZ = 190, ToX = -1648, ToY = 3072, ToZ = 120}         -- "Flame Arrow"
  4:  Exit  {}
end

event 44
  0:  OnTimer  {IntervalInHalfMinutes = 8}
  1:  Cmp  {"MapVar6", Value = 1,   jump = 4}
  2:  CastSpell  {Spell = 13, Mastery = const.Novice, Skill = 2, FromX = -2040, FromY = 3456, FromZ = 120, ToX = -8, ToY = 3456, ToZ = 120}         -- "Static Charge"
  3:  CastSpell  {Spell = 13, Mastery = const.Novice, Skill = 2, FromX = -8, FromY = 3456, FromZ = 120, ToX = -2040, ToY = 3456, ToZ = 120}         -- "Static Charge"
  4:  Exit  {}
end

event 45
  0:  OnTimer  {IntervalInHalfMinutes = 7}
  1:  Cmp  {"MapVar9", Value = 1,   jump = 4}
  2:  CastSpell  {Spell = 24, Mastery = const.Novice, Skill = 2, FromX = -2040, FromY = 3840, FromZ = 120, ToX = -8, ToY = 3840, ToZ = 120}         -- "Cold Beam"
  3:  CastSpell  {Spell = 24, Mastery = const.Novice, Skill = 2, FromX = -8, FromY = 3840, FromZ = 120, ToX = -2040, ToY = 3840, ToZ = 120}         -- "Cold Beam"
  4:  Exit  {}
end

event 46
  0:  OnTimer  {IntervalInHalfMinutes = 6}
  1:  Cmp  {"MapVar12", Value = 1,   jump = 4}
  2:  CastSpell  {Spell = 35, Mastery = const.Novice, Skill = 2, FromX = -2040, FromY = 4096, FromZ = 120, ToX = -8, ToY = 4096, ToZ = 120}         -- "Magic Arrow"
  3:  CastSpell  {Spell = 35, Mastery = const.Novice, Skill = 2, FromX = -8, FromY = 4096, FromZ = 120, ToX = -2040, ToY = 4096, ToZ = 120}         -- "Magic Arrow"
  4:  Exit  {}
end

event 53
      Hint = str[8]  -- "Exit"
  0:  MoveToMap  {X = 15218, Y = -14862, Z = 161, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 6, Name = "OutC1.odm"}
end

event 54
      Hint = str[14]  -- "Bag"
  0:  OpenChest  {Id = 2}
end

event 55
      Hint = str[14]  -- "Bag"
  0:  OpenChest  {Id = 3}
end

event 56
      Hint = str[14]  -- "Bag"
  0:  OpenChest  {Id = 4}
end

event 57
      Hint = str[14]  -- "Bag"
  0:  OpenChest  {Id = 5}
end

event 58
      Hint = str[14]  -- "Bag"
  0:  OpenChest  {Id = 6}
end

event 61
      Hint = str[16]  -- "Magic Door"
  0:  SetMessage  {Str = 18}         -- "You cannot see me, hear me or touch me.  I lie behind the stars and alter what is real, I am what you really fear, Close your eyes and come I near. What am I?"
  1:  Question  {Question = 21, Answer1 = 19, Answer2 = 20,   jump(ok) = 4}         -- "Answer?  " ("dark", "darkness")
  2:  StatusText  {Str = 22}         -- "Incorrect"
  3:  Exit  {}

  4:  SetDoorState  {Id = 61, State = 1}
end

event 62
      Hint = str[16]  -- "Magic Door"
  0:  SetMessage  {Str = 23}         -- "I go through an apple, or point out your way, I fit in a bow, then a target, to stay. What am I?"
  1:  Question  {Question = 21, Answer1 = 24, Answer2 = 25,   jump(ok) = 4}         -- "Answer?  " ("arrow", "an arrow")
  2:  StatusText  {Str = 22}         -- "Incorrect"
  3:  Exit  {}

  4:  SetDoorState  {Id = 62, State = 1}
end

event 63
      Hint = str[16]  -- "Magic Door"
  0:  SetMessage  {Str = 26}         -- "What consumes rocks, levels mountains, destroys wood, pushes the clouds across the sky, and can make a young one old?"
  1:  Question  {Question = 21, Answer1 = 27, Answer2 = 27,   jump(ok) = 4}         -- "Answer?  " ("time")
  2:  StatusText  {Str = 22}         -- "Incorrect"
  3:  Exit  {}

  4:  SetDoorState  {Id = 63, State = 1}
end

event 64
      Hint = str[16]  -- "Magic Door"
  0:  SetMessage  {Str = 28}         -- "Alive without breath, as cold as death, never thirsty ever drinking, all in mail never clinking, ever travelling, never walking, mouth ever moving, never talking.  What am I? "
  1:  Question  {Question = 21, Answer1 = 29, Answer2 = 30,   jump(ok) = 4}         -- "Answer?  " ("fish", "a fish")
  2:  StatusText  {Str = 22}         -- "Incorrect"
  3:  Exit  {}

  4:  SetDoorState  {Id = 64, State = 1}
end

event 65
  0:  MoveToMap  {X = 0, Y = 0, Z = 0, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "d03.blv"}
  1:  Subtract  {"Food", Value = 5}
end

event 66
  0:  MoveToMap  {X = 1408, Y = -1664, Z = 1, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 67
  0:  MoveToMap  {X = 0, Y = 0, Z = 0, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "d06.blv"}
  1:  Subtract  {"Food", Value = 5}
end

event 68
  0:  MoveToMap  {X = 11407, Y = 18074, Z = 1, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "outc2.odm"}
  1:  Subtract  {"Food", Value = 5}
end

event 69
      Hint = str[31]  -- "Magical Wall"
  0:  Exit  {}
end

event 70
      Hint = str[32]  -- "Sack"
  0:  Cmp  {"MapVar50", Value = 1,   jump = 4}
  1:  Set  {"MapVar50", Value = 1}
  2:  Add  {"FoodAddRandom", Value = 4}
  3:  Exit  {}

  4:  StatusText  {Str = 33}         -- "Empty"
end

event 71
      Hint = str[32]  -- "Sack"
  0:  Cmp  {"MapVar51", Value = 1,   jump = 4}
  1:  Set  {"MapVar51", Value = 1}
  2:  Add  {"FoodAddRandom", Value = 4}
  3:  Exit  {}

  4:  StatusText  {Str = 33}         -- "Empty"
end

event 72
      Hint = str[32]  -- "Sack"
  0:  Cmp  {"MapVar52", Value = 1,   jump = 4}
  1:  Set  {"MapVar52", Value = 1}
  2:  Add  {"FoodAddRandom", Value = 4}
  3:  Exit  {}

  4:  StatusText  {Str = 33}         -- "Empty"
end

event 73
      Hint = str[32]  -- "Sack"
  0:  Cmp  {"MapVar53", Value = 1,   jump = 3}
  1:  Set  {"MapVar53", Value = 1}
  2:  Add  {"FoodAddRandom", Value = 3}
  3:  Exit  {}

  4:  StatusText  {Str = 33}         -- "Empty"
end

event 74
      Hint = str[17]  -- "Bag"
  0:  OpenChest  {Id = 7}
end

event 75
      Hint = str[17]  -- "Bag"
  0:  OpenChest  {Id = 8}
end

event 76
      Hint = str[17]  -- "Bag"
  0:  OpenChest  {Id = 9}
end

event 77
      Hint = str[17]  -- "Bag"
  0:  OpenChest  {Id = 10}
end

event 78
      Hint = str[17]  -- "Bag"
  0:  OpenChest  {Id = 11}
end

event 79
      Hint = str[17]  -- "Bag"
  0:  OpenChest  {Id = 12}
end

event 80
      Hint = str[17]  -- "Bag"
  0:  StatusText  {Str = 33}         -- "Empty"
end
