str[0] = " "
str[1] = "There is a strong smell of gas pervading the air."
str[2] = "Small glittering pool."
str[3] = "The water cools you down."
str[4] = "Switch."
str[5] = "The torches Ignite and so does the swamp gas."
str[6] = "Hundreds of snakes crawl out of the floor and bite you."
str[7] = "Swamp gas forces you back couching and gagging."
str[8] = "You drink the water, with no effect."
str[9] = "The switch does not work."
str[10] = "Hundreds of snakes crawl out of the floor and bite you. "
str[11] = "A voice hisses -Walk the path of the snake-."
str[12] = "A voice whispers -I am the spirit of the swamp, congratulations, take what you may and leave in peace-"
str[13] = ""


event 1
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  StatusText  {Str = 1}         -- "There is a strong smell of gas pervading the air."
  2:  Add  {"MapVar0", Value = 1}
  3:  Exit  {}
end

event 2
      Hint = str[2]  -- "Small glittering pool."
  0:  Cmp  {"MapVar1", Value = 4,   jump = 5}
  1:  StatusText  {Str = 3}         -- "The water cools you down."
  2:  Add  {"FireResBonus", Value = 40}
  3:  Add  {"MapVar1", Value = 1}
  4:  Exit  {}

  5:  StatusText  {Str = 8}         -- "You drink the water, with no effect."
end

event 3
      Hint = str[4]  -- "Switch."
  0:  Cmp  {"MapVar2", Value = 1,   jump = 3}
  1:  StatusText  {Str = 5}         -- "The torches Ignite and so does the swamp gas."
  2:  GoTo  {jump = 5}

  3:  StatusText  {Str = 9}         -- "The switch does not work."
  4:  Exit  {}

  5:  SetTexture  {Facet = 225, Name = "d8s2on"}
  6:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 0, FromX = -3004, FromY = -2315, FromZ = 120, ToX = -3244, ToY = -2315, ToZ = 120}         -- "Fireball"
  7:  DamagePlayer  {Player = "All", DamageType = const.Damage.Magic, Damage = 20}
  8:  SetSprite  {SpriteId = 1, Visible = 1, Name = "torch01"}
  9:  SetSprite  {SpriteId = 2, Visible = 1, Name = "torch01"}
  10: SetSprite  {SpriteId = 3, Visible = 1, Name = "torch01"}
  11: SetSprite  {SpriteId = 4, Visible = 1, Name = "torch01"}
  12: SetSprite  {SpriteId = 5, Visible = 1, Name = "torch01"}
  13: SetSprite  {SpriteId = 6, Visible = 1, Name = "torch01"}
  14: SetSprite  {SpriteId = 7, Visible = 1, Name = "torch01"}
  15: SetSprite  {SpriteId = 8, Visible = 1, Name = "torch01"}
  16: SetSprite  {SpriteId = 9, Visible = 1, Name = "torch01"}
  17: SetSprite  {SpriteId = 10, Visible = 1, Name = "torch01"}
  18: SetSprite  {SpriteId = 11, Visible = 1, Name = "torch01"}
  19: SetSprite  {SpriteId = 12, Visible = 1, Name = "torch01"}
  20: SetSprite  {SpriteId = 13, Visible = 1, Name = "torch01"}
  21: SetSprite  {SpriteId = 14, Visible = 1, Name = "torch01"}
  22: SetSprite  {SpriteId = 15, Visible = 1, Name = "torch01"}
  23: SetSprite  {SpriteId = 16, Visible = 1, Name = "torch01"}
  24: SetSprite  {SpriteId = 17, Visible = 1, Name = "torch01"}
  25: SetSprite  {SpriteId = 18, Visible = 1, Name = "torch01"}
  26: SetSprite  {SpriteId = 19, Visible = 1, Name = "torch01"}
  27: SetSprite  {SpriteId = 20, Visible = 1, Name = "torch01"}
  28: SetSprite  {SpriteId = 21, Visible = 1, Name = "torch01"}
  29: SetSprite  {SpriteId = 22, Visible = 1, Name = "torch01"}
  30: SetSprite  {SpriteId = 23, Visible = 1, Name = "torch01"}
  31: SetSprite  {SpriteId = 24, Visible = 1, Name = "torch01"}
  32: Set  {"MapVar2", Value = 1}
  33: Set  {"MapVar3", Value = 1}
end

event 5
  0:  Cmp  {"MapVar3", Value = 1,   jump = 4}
  1:  StatusText  {Str = 7}         -- "Swamp gas forces you back couching and gagging."
  2:  DamagePlayer  {Player = "All", DamageType = const.Damage.Cold, Damage = 8}
  3:  MoveToMap  {X = -1328, Y = -704, Z = 0, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  4:  Exit  {}
end

event 6
  0:  Cmp  {"MapVar3", Value = 1,   jump = 4}
  1:  StatusText  {Str = 7}         -- "Swamp gas forces you back couching and gagging."
  2:  DamagePlayer  {Player = "All", DamageType = const.Damage.Cold, Damage = 8}
  3:  MoveToMap  {X = -2128, Y = -1152, Z = 0, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  4:  Exit  {}
end

event 7
  0:  Cmp  {"MapVar3", Value = 1,   jump = 4}
  1:  StatusText  {Str = 7}         -- "Swamp gas forces you back couching and gagging."
  2:  DamagePlayer  {Player = "All", DamageType = const.Damage.Cold, Damage = 8}
  3:  MoveToMap  {X = -1888, Y = -1600, Z = 0, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  4:  Exit  {}
end

event 8
  0:  StatusText  {Str = 6}         -- "Hundreds of snakes crawl out of the floor and bite you."
  1:  DamagePlayer  {Player = "All", DamageType = const.Damage.Cold, Damage = 15}
end

event 9
  0:  Cmp  {"MapVar4", Value = 1,   jump = 3}
  1:  StatusText  {Str = 11}         -- "A voice hisses -Walk the path of the snake-."
  2:  Add  {"MapVar4", Value = 1}
  3:  Exit  {}
end

event 10
  0:  Cmp  {"MapVar5", Value = 1,   jump = 6}
  1:  SetMessage  {Str = 12}         -- "A voice whispers -I am the spirit of the swamp, congratulations, take what you may and leave in peace-"
  2:  SimpleMessage  {}
  3:  ForPlayer  ("All")
  4:  Add  {"Experience", Value = 750}
  5:  Add  {"MapVar5", Value = 1}
  6:  Exit  {}
end
