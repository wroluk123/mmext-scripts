event 1
  4:  Exit  {}

  15: Exit  {}
end

event 2
  4:  EnterHouse  {Id = 251658757}         -- not found!
  15: EnterHouse  {Id = 197634}         -- not found!
end

event 3
  4:  PlaySound  {Id = 251659013, X = 67371523, Y = 84149248}
  15: PlaySound  {Id = 263170, X = 67437572, Y = 33820416}
end

event 4
  4:  Hint  {Str = 5}
  15: Hint  {Str = 2}
end

event 5
  4:  MazeInfo  {Str = 5}
  15: MazeInfo  {Str = 2}
end

event 6
  4:  MoveToMap  {X = 251659781, Y = 117703174, Z = 84345856, Direction = 118423559, LookAngle = 525314, SpeedZ = 134547460, HouseId = 0, Icon = 15, Name = "\8\2\4\9"}
  15: MoveToMap  {X = 459778, Y = 117769988, Z = 34017024, Direction = 67110916, LookAngle = 525576, SpeedZ = 67241999, HouseId = 9, Icon = 0, Name = "\4\9\5\9"}         -- "Haft and Handle Pole arms"
end

event 7
  4:  OpenChest  {Id = 5}
  15: OpenChest  {Id = 2}
end

event 8
  4:  FaceExpression  {Player = "All", Frame = 8}
  15: FaceExpression  {Player = 2, Frame = 4}
end

event 9
  4:  DamagePlayer  {Player = "All", -- ERROR: Const not found
DamageType = 9, Damage = 34148096}
  15: DamagePlayer  {Player = 2, DamageType = const.Damage.Cold, Damage = 168034314}
end

event 10
  4:  SetSnow  {EffectId = 5, On = true}
  15: SetSnow  {EffectId = 2, On = true}
end

event 11
  4:  SetTexture  {Facet = 251661061, Name = "\11\2\4\12"}
  15: SetTexture  {Facet = 787458, Name = "\4\12\5\12"}
end

event 12
  4:  SetTextureOutdoors  {Model = 251661317, Facet = 218366476, Name = ""}
  15: SetTextureOutdoors  {Model = 852994, Facet = 218434820, Name = ""}
end

event 13
  4:  SetSprite  {SpriteId = 251661573, Visible = 13, Name = "\2\4\14"}
  15: SetSprite  {SpriteId = 918530, Visible = 4, Name = "\14\5\14"}
end

event 14
  4:  Cmp  {"SP", Value = 235864078,   jump = 2}
  15: Cmp  {-- ERROR: Const not found
"ClassIs", Value = 67112708,   jump = 15}
end

event 15
  4:  SetDoorState  {Id = 5, State = 15}
  15: SetDoorState  {Id = 2, State = 4}
end

event 16
  4:  Add  {"SP", Value = 269418512}
  15: Add  {-- ERROR: Const not found
"ClassIs", Value = 67113220}
end

event 17
  4:  Subtract  {"SP", Value = 286195729}
  15: Subtract  {-- ERROR: Const not found
"ClassIs", Value = 67113476}
end

event 18
  4:  Set  {"SP", Value = 302972946}
  15: Set  {-- ERROR: Const not found
"ClassIs", Value = 67113732}
end

event 19
  4:  SummonMonsters  {TypeIndexInMapStats = 5, Level = 19, Count = 0, X = 67244815, Y = 335806484, Z = 251663365}
  15: SummonMonsters  {TypeIndexInMapStats = 2, Level = 4, Count = 20, X = 85197824, Y = 336527380, Z = 1377282}
end

event 20
  4:  Cmd14  {unk_1 = 251663365, unk_2 = 20}
  15: Cmd14  {unk_1 = 1377282, unk_2 = 4}
end

event 21
  4:  CastSpell  {Spell = 5, Mastery = 22, Skill = 0, FromX = 67245327, FromY = 369360918, FromZ = 251663877, ToX = 386138646, ToY = 85394432, ToZ = 386859031}         -- "Haste"
  15: CastSpell  {Spell = 2, Mastery = 5, Skill = 22, FromX = 85328896, FromY = 370081814, FromZ = 1508354, ToX = 386209540, ToY = 35065600, ToZ = 67115012}         -- "Flame Arrow"
end

event 22
  4:  SpeakNPC  {NPC = 251663877}         -- not found!
  15: SpeakNPC  {NPC = 1508354}         -- not found!
end

event 23
  4:  SetFacetBit  {Id = 251664133, -- ERROR: Const not found
-- ERROR: Const not found
Bit = const.FacetBits.TriggerByObject + const.FacetBits.TriggerByMonster + const.FacetBits.MoveByDoor + const.FacetBits.ProjectToXZ + const.FacetBits.IsWater + 0x4 + 0x2 + const.FacetBits.IsPortal, On = false}
  15: SetFacetBit  {Id = 1573890, -- ERROR: Const not found
-- ERROR: Const not found
-- ERROR: Const not found
-- ERROR: Const not found
Bit = const.FacetBits.TriggerByObject + const.FacetBits.TriggerByMonster + const.FacetBits.MoveByDoor + 0x10000 + 0x1000 + 0x800 + 0x4, On = false}
end

event 24
  4:  SetFacetBitOutdoors  {Model = 251664389, Facet = 419693080, -- ERROR: Const not found
-- ERROR: Const not found
Bit = const.FacetBits.TriggerByStep + const.FacetBits.FlipV + const.FacetBits.IsEventJustHint + 0x80000 + 0x10000 + const.FacetBits.ProjectToYZ, On = true}
  15: SetFacetBitOutdoors  {Model = 1639426, Facet = 419764484, -- ERROR: Const not found
-- ERROR: Const not found
-- ERROR: Const not found
Bit = const.FacetBits.TriggerByClick + const.FacetBits.IsEventJustHint + 0x80000 + 0x10000 + 0x800 + const.FacetBits.ProjectToYZ + const.FacetBits.ProjectToXZ + const.FacetBits.ProjectToXY, On = true}
end

event 25
  4:  RandomGoTo  {jumpA = 5, jumpB = 25, jumpC = 0, jumpD = 15, jumpE = 25, jumpF = 2}
  15: RandomGoTo  {jumpA = 2, jumpB = 4, jumpC = 26, jumpD = 0, jumpE = 4, jumpF = 26}
end

event 26
  4:  Question  {Question = 251664901, Answer1 = 453247514, Answer2 = 85656576,   jump(ok) = 27}
  15: Question  {Question = 1770498, Answer1 = 453319428, Answer2 = 35327744,   jump(ok) = 4}
end

event 27
  4:  Cmd1B  {unk_1 = 5, unk_2 = 27}
  15: Cmd1B  {unk_1 = 2, unk_2 = 4}
end

event 28
  4:  Cmd1C  {unk_1 = 5}
  15: Cmd1C  {unk_1 = 2}
end

event 29
  4:  StatusText  {Str = 251665669}
  15: StatusText  {Str = 1967106}
end

event 30
  4:  SetMessage  {Str = 251665925}
  15: SetMessage  {Str = 2032642}
end

event 31
  4:  OnTimer  {EachYear = true, EachMonth = true, Hour = 15, Minute = 31, Second = 2, IntervalInHalfMinutes = 7941}

  15: OnTimer  {EachYear = true, EachMonth = true, EachWeek = true, Hour = 1, Minute = 15, Second = 32, IntervalInHalfMinutes = 1026}
end

event 287
  15: SetLight  {Id = 2098178, On = true}
end

event 32
  4:  SetLight  {Id = 251666437, On = true}
  15: SimpleMessage  {}
end

event 288
  15: SummonObject  {Type = 2163714, X = 553984260, Y = 35852032, Z = 251732229, Speed = 570688036, Count = 0, RandomAngle = true}         -- not found!
end

event 33
  4:  SimpleMessage  {}
  15: ForPlayer  (2)
end

event 289
  15: GoTo  {jump = 2}
end

event 34
  4:  SummonObject  {Type = 251666949, X = 570753573, Y = 36048641, Z = 67117828, Speed = 2295075, Count = 15, RandomAngle = true}         -- not found!

  15: OnLoadMap  {}
end

event 290
  15: OnRefillTimer  {EachYear = true, EachMonth = true, EachWeek = true, Minute = 4, Second = 35, IntervalInHalfMinutes = 8965}
end

event 35
  4:  ForPlayer  ("All")
  15: SetNPCTopic  {NPC = 19072258, Index = 15, Event = 604242472}         -- not found! : not found!
end

event 291
  15: MoveNPC  {NPC = 2360322, HouseId = 604316676}         -- not found! -> not found!
end

event 36
  4:  GoTo  {jump = 5}

  15: GiveItem  {Strength = 2, Type = const.ItemType.Shield, Id = 705626404}         -- not found!
end

event 292
  15: ChangeEvent  {NewEvent = 2425858}         -- not found!
end

event 37
  4:  OnLoadMap  {}
  15: CheckSkill  {const.Skills.Dagger, Mastery = 6, Level = 739180837,   jump(>=) = 2}
end

event 293
end

event 38
  4:  OnRefillTimer  {EachYear = true, EachMonth = true, Hour = 15, Minute = 45, Second = 2, IntervalInHalfMinutes = 9733}
end

event 294
end

event 39
  4:  SetNPCTopic  {NPC = 251668229, Index = 47, Event = 19334402}         -- not found! : not found!
end

event 295
end

event 551
end

event 40
  4:  MoveNPC  {NPC = 251668485, HouseId = 671416882}         -- not found! -> not found!
end

event 296
end

event 552
end

event 808
end
