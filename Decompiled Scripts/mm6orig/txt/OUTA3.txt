str[0] = " "
str[1] = "Drink from Fountain"
str[2] = "Rejuvination!"
str[3] = "Refreshing!"
str[4] = "Chest"
str[5] = "Hermit's Isle"
str[6] = "The surface of the obelisk is blood warm to the touch.  A message swims into view as you remove your hand:                                                                                                                                                            _etecpe__ersoede"
str[7] = "Obelisk"
str[8] = ""


event 1
      Hint = str[7]  -- "Obelisk"
      MazeInfo = str[5]  -- "Hermit's Isle"
  0:  Exit  {}
end

event 75
      Hint = str[4]  -- "Chest"
  0:  OpenChest  {Id = 1}
end

event 76
      Hint = str[4]  -- "Chest"
  0:  OpenChest  {Id = 2}
end

event 90
  0:  MoveToMap  {X = -2048, Y = 3453, Z = 2049, Direction = 1536, LookAngle = 0, SpeedZ = 0, HouseId = 196, Icon = 5, Name = "T6.Blv"}         -- "Supreme Temple of Baa"
  1:  Exit  {}

  2:  EnterHouse  {Id = 196}         -- "Supreme Temple of Baa"
end

event 100
      Hint = str[1]  -- "Drink from Fountain"
  0:  Set  {"AgeBonus", Value = 0}
  1:  StatusText  {Str = 2}         -- "Rejuvination!"
  2:  Set  {"AutonotesBits", Value = 51}         -- "Unnatural aging cured at fountain to the east of Hermit's Isle."
end

event 210
      Hint = str[7]  -- "Obelisk"
  0:  SetMessage  {Str = 6}         -- "The surface of the obelisk is blood warm to the touch.  A message swims into view as you remove your hand:                                                                                                                                                            _etecpe__ersoede"
  1:  SimpleMessage  {}
  2:  Set  {"QBits", Value = 362}         -- NPC
  3:  Set  {"AutonotesBits", Value = 81}         -- "Obelisk Message # 3:  _etecpe__ersoede"
end
