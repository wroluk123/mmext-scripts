str[0] = " "
str[1] = "Door"
str[2] = "Chest"
str[3] = "All must have amber.  Take life force!"
str[4] = "The chest is locked."
str[5] = "Magically Refreshing!"
str[6] = "Exit"
str[7] = "Deleted"
str[8] = "The door is locked."
str[9] = "Ah, delicious amber!"
str[10] = "Stone Face"
str[11] = "Water"
str[12] = "Bag"
str[13] = "The Door is warded."
str[14] = "All wards must be destroyed."
str[15] = "The door won't budge."
str[16] = "Lord of Fire"
str[17] = "Hall of the Fire Lord"
str[18] = ""


event 2
      Hint = str[1]  -- "Door"
      MazeInfo = str[17]  -- "Hall of the Fire Lord"
  0:  SetDoorState  {Id = 2, State = 1}
end

event 3
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 3, State = 1}
end

event 4
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 4, State = 1}
end

event 5
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 5, State = 1}
end

event 6
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 6, State = 1}
end

event 7
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 7, State = 1}
end

event 8
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 8, State = 1}
end

event 9
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 9, State = 1}
end

event 10
      Hint = str[1]  -- "Door"
  0:  Cmp  {"QBits", Value = 71,   jump = 14}         -- Walt
  1:  Cmp  {"MapVar9", Value = 1,   jump = 10}
  2:  Cmp  {"QBits", Value = 41,   jump = 5}         -- Bruce
  3:  StatusText  {Str = 13}         -- "The Door is warded."
  4:  Exit  {}

  5:  Cmp  {"Inventory", Value = 482,   jump = 8}         -- "Amber"
  6:  StatusText  {Str = 13}         -- "The Door is warded."
  7:  Exit  {}

  8:  Subtract  {"Inventory", Value = 482}         -- "Amber"
  9:  Set  {"MapVar9", Value = 1}
  10: Cmp  {"MapVar19", Value = 1,   jump = 13}
  11: StatusText  {Str = 14}         -- "All wards must be destroyed."
  12: Exit  {}

  13: Set  {"QBits", Value = 71}         -- Walt
  14: SetDoorState  {Id = 10, State = 1}
  15: SetDoorState  {Id = 20, State = 1}
end

event 11
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 11, State = 1}
end

event 12
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 12, State = 1}
end

event 13
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 13, State = 1}
end

event 14
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 14, State = 1}
end

event 15
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 15, State = 1}
end

event 16
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 16, State = 1}
end

event 17
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 17, State = 1}
end

event 18
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 18, State = 1}
  1:  SetDoorState  {Id = 45, State = 0}
  2:  SetDoorState  {Id = 46, State = 0}
end

event 19
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 19, State = 1}
end

event 20
      Hint = str[1]  -- "Door"
  0:  Cmp  {"QBits", Value = 71,   jump = 14}         -- Walt
  1:  Cmp  {"MapVar19", Value = 1,   jump = 10}
  2:  Cmp  {"QBits", Value = 41,   jump = 5}         -- Bruce
  3:  StatusText  {Str = 13}         -- "The Door is warded."
  4:  Exit  {}

  5:  Cmp  {"Inventory", Value = 482,   jump = 8}         -- "Amber"
  6:  StatusText  {Str = 13}         -- "The Door is warded."
  7:  Exit  {}

  8:  Subtract  {"Inventory", Value = 482}         -- "Amber"
  9:  Set  {"MapVar19", Value = 1}
  10: Cmp  {"MapVar9", Value = 1,   jump = 13}
  11: StatusText  {Str = 14}         -- "All wards must be destroyed."
  12: Exit  {}

  13: Set  {"QBits", Value = 71}         -- Walt
  14: SetDoorState  {Id = 20, State = 1}
end

event 21
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 21, State = 1}
end

event 22
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 22, State = 1}
end

event 23
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 23, State = 1}
end

event 25
  0:  SetDoorState  {Id = 25, State = 2}         -- switch state
end

event 27
      Hint = str[10]  -- "Stone Face"
  0:  ForPlayer  (0)
  1:  Cmp  {"Inventory", Value = 482,   jump = 4}         -- "Amber"
  2:  DamagePlayer  {Player = 0, DamageType = const.Damage.Phys, Damage = 50}
  3:  Set  {"MapVar0", Value = 1}
  4:  ForPlayer  (1)
  5:  Cmp  {"Inventory", Value = 482,   jump = 8}         -- "Amber"
  6:  DamagePlayer  {Player = 1, DamageType = const.Damage.Phys, Damage = 50}
  7:  Set  {"MapVar0", Value = 1}
  8:  ForPlayer  (2)
  9:  Cmp  {"Inventory", Value = 482,   jump = 12}         -- "Amber"
  10: DamagePlayer  {Player = 2, DamageType = const.Damage.Phys, Damage = 50}
  11: Set  {"MapVar0", Value = 1}
  12: ForPlayer  (3)
  13: Cmp  {"Inventory", Value = 482,   jump = 16}         -- "Amber"
  14: DamagePlayer  {Player = 3, DamageType = const.Damage.Phys, Damage = 50}
  15: Set  {"MapVar0", Value = 1}
  16: Cmp  {"MapVar0", Value = 1,   jump = 20}
  17: StatusText  {Str = 9}         -- "Ah, delicious amber!"
  18: MoveToMap  {X = -1792, Y = -19, Z = 1, Direction = 1, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  19: Exit  {}

  20: StatusText  {Str = 3}         -- "All must have amber.  Take life force!"
  21: Set  {"MapVar0", Value = 0}
  22: MoveToMap  {X = -1792, Y = -19, Z = 1, Direction = 1, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  23: Exit  {}
end

event 28
  0:  MoveToMap  {X = -2853, Y = 1600, Z = -2655, Direction = 1024, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 29
  0:  MoveToMap  {X = 2823, Y = 1534, Z = -2655, Direction = 45, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 30
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 30, State = 1}
end

event 31
  0:  SetDoorState  {Id = 30, State = 0}
end

event 32
  0:  SetDoorState  {Id = 45, State = 1}
  1:  SetDoorState  {Id = 46, State = 1}
end

event 33
  0:  SetDoorState  {Id = 18, State = 0}
end

event 41
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 1}
end

event 42
      Hint = str[2]  -- "Chest"
  0:  Cmp  {"MapVar10", Value = 1,   jump = 9}
  1:  Cmp  {"QBits", Value = 75,   jump = 6}         -- Lisa
  2:  Cmp  {"Inventory", Value = 559,   jump = 5}         -- "Chest Key"
  3:  StatusText  {Str = 4}         -- "The chest is locked."
  4:  Exit  {}

  5:  Subtract  {"Inventory", Value = 559}         -- "Chest Key"
  6:  OpenChest  {Id = 2}
  7:  Set  {"QBits", Value = 75}         -- Lisa
  8:  Exit  {}

  9:  OpenChest  {Id = 12}
end

event 43
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 3}
end

event 44
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 4}
end

event 45
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 5}
end

event 46
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 6}
end

event 47
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 7}
end

event 48
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 8}
end

event 49
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 9}
end

event 50
  0:  Cmp  {"MapVar1", Value = 1,   jump = 4}
  1:  CastSpell  {Spell = 83, Mastery = const.Novice, Skill = 4, FromX = 0, FromY = 0, FromZ = 0, ToX = 0, ToY = 0, ToZ = 0}         -- "Day of the Gods"
  2:  Set  {"MapVar1", Value = 1}
  3:  StatusText  {Str = 5}         -- "Magically Refreshing!"
  4:  Exit  {}
end

event 51
      Hint = str[6]  -- "Exit"
  0:  MoveToMap  {X = 16387, Y = -20005, Z = 225, Direction = 512, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 6, Name = "OutD2.Odm"}
end

event 52
      Hint = str[16]  -- "Lord of Fire"
  0:  SetDoorState  {Id = 51, State = 1}
  1:  SetDoorState  {Id = 52, State = 1}
  2:  SpeakNPC  {NPC = 298}         -- "Lord of Fire"
  3:  Set  {"QBits", Value = 41}         -- Bruce
end

event 53
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 10}
end

event 54
      Hint = str[10]  -- "Stone Face"
  0:  ForPlayer  (0)
  1:  Cmp  {"Inventory", Value = 482,   jump = 4}         -- "Amber"
  2:  DamagePlayer  {Player = 0, DamageType = const.Damage.Phys, Damage = 50}
  3:  Set  {"MapVar0", Value = 1}
  4:  ForPlayer  (1)
  5:  Cmp  {"Inventory", Value = 482,   jump = 8}         -- "Amber"
  6:  DamagePlayer  {Player = 1, DamageType = const.Damage.Phys, Damage = 50}
  7:  Set  {"MapVar0", Value = 1}
  8:  ForPlayer  (2)
  9:  Cmp  {"Inventory", Value = 482,   jump = 12}         -- "Amber"
  10: DamagePlayer  {Player = 2, DamageType = const.Damage.Phys, Damage = 50}
  11: Set  {"MapVar0", Value = 1}
  12: ForPlayer  (3)
  13: Cmp  {"Inventory", Value = 482,   jump = 16}         -- "Amber"
  14: DamagePlayer  {Player = 3, DamageType = const.Damage.Phys, Damage = 50}
  15: Set  {"MapVar0", Value = 1}
  16: Cmp  {"MapVar0", Value = 1,   jump = 20}
  17: StatusText  {Str = 9}         -- "Ah, delicious amber!"
  18: MoveToMap  {X = -1792, Y = -19, Z = 1, Direction = 1, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  19: Exit  {}

  20: StatusText  {Str = 3}         -- "All must have amber.  Take life force!"
  21: Set  {"MapVar0", Value = 0}
  22: MoveToMap  {X = -1792, Y = -19, Z = 1, Direction = 1, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  23: Exit  {}
end

event 55
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 55, State = 1}
end

event 56
  0:  OnTimer  {Hour = 12}
  1:  Set  {"MapVar1", Value = 0}
end

event 57
  0:  OnRefillTimer  {Hour = 12}
  1:  Set  {"MapVar1", Value = 0}
end

event 58
      Hint = str[10]  -- "Stone Face"
  0:  ForPlayer  (0)
  1:  Cmp  {"Inventory", Value = 482,   jump = 4}         -- "Amber"
  2:  DamagePlayer  {Player = 0, DamageType = const.Damage.Phys, Damage = 50}
  3:  Set  {"MapVar0", Value = 1}
  4:  ForPlayer  (1)
  5:  Cmp  {"Inventory", Value = 482,   jump = 8}         -- "Amber"
  6:  DamagePlayer  {Player = 1, DamageType = const.Damage.Phys, Damage = 50}
  7:  Set  {"MapVar0", Value = 1}
  8:  ForPlayer  (2)
  9:  Cmp  {"Inventory", Value = 482,   jump = 12}         -- "Amber"
  10: DamagePlayer  {Player = 2, DamageType = const.Damage.Phys, Damage = 50}
  11: Set  {"MapVar0", Value = 1}
  12: ForPlayer  (3)
  13: Cmp  {"Inventory", Value = 482,   jump = 16}         -- "Amber"
  14: DamagePlayer  {Player = 3, DamageType = const.Damage.Phys, Damage = 50}
  15: Set  {"MapVar0", Value = 1}
  16: Cmp  {"MapVar0", Value = 1,   jump = 20}
  17: StatusText  {Str = 9}         -- "Ah, delicious amber!"
  18: MoveToMap  {X = -1792, Y = -19, Z = 1, Direction = 1, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  19: Exit  {}

  20: StatusText  {Str = 3}         -- "All must have amber.  Take life force!"
  21: Set  {"MapVar0", Value = 0}
  22: MoveToMap  {X = -1792, Y = -19, Z = 1, Direction = 1, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  23: Exit  {}
end

event 59
      Hint = str[12]  -- "Bag"
  0:  OpenChest  {Id = 11}
end

event 60
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 58, State = 1}
end

event 61
  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 71,   jump = 3}         -- Walt
  2:  GoTo  {jump = 3}

  3:  Cmp  {"QBits", Value = 75,   jump = 5}         -- Lisa
  4:  Exit  {}

  5:  Set  {"MapVar10", Value = 1}
end
