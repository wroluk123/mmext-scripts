str[0] = " "
str[1] = "Door"
str[2] = "Bed"
str[3] = "Chest"
str[4] = "Cabinet"
str[5] = "Sewer Grate"
str[6] = "Someone's hiding under the bed!"
str[7] = "Torch"
str[8] = "Trash"
str[9] = "Something's stashed here!"
str[10] = "Ouch!"
str[11] = "The door is locked."
str[12] = "Exit"
str[13] = "Cylinder"
str[14] = "+10 Hit points restored."
str[15] = "Refreshing!"
str[16] = "Well"
str[17] = "The Underground"
str[18] = "Strange Light"
str[19] = "The ladder is broken!"
str[20] = "Caged Prisoner"
str[21] = "What took you so long?"
str[22] = "Antonio's Venetian Tea"
str[23] = "The chest is locked!"
str[24] = ""


event 1
  110: GoTo  {jump = 0}

  0:  OnLoadMap  {}
  1:  ForPlayer  ("All")
  2:  Set  {"Eradicated", Value = 0}
end
