str[0] = " "
str[1] = "Exit Door"
str[2] = "Chest"
str[3] = "Switch"
str[4] = "Empty"
str[5] = "Empty"
str[6] = "Empty"
str[7] = "Door"
str[8] = "A"
str[9] = "B"
str[10] = "C"
str[11] = "D"
str[12] = "E"
str[13] = "F"
str[14] = "G"
str[15] = "H"
str[16] = "I"
str[17] = "J"
str[18] = "K"
str[19] = "L"
str[20] = "M"
str[21] = "N"
str[22] = "O"
str[23] = "P"
str[24] = "GoblinWatch"
str[25] = ""


event 1
      Hint = str[7]  -- "Door"
      MazeInfo = str[24]  -- "GoblinWatch"
  0:  SetDoorState  {Id = 1, State = 1}
end

event 2
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 2, State = 1}
end

event 3
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 3, State = 1}
end

event 4
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 4, State = 1}
end

event 5
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 5, State = 1}
end

event 6
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 6, State = 1}
end

event 7
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 7, State = 1}
end

event 8
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 8, State = 1}
end

event 9
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 9, State = 1}
end

event 10
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 10, State = 1}
end

event 11
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 11, State = 1}
end

event 12
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 12, State = 1}
end

event 13
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 13, State = 1}
end

event 14
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 14, State = 1}
end

event 15
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 15, State = 1}
end

event 16
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 16, State = 1}
end

event 17
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 17, State = 1}
end

event 18
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 18, State = 1}
end

event 19
      Hint = str[8]  -- "A"
  0:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 3, FromX = 2496, FromY = 4864, FromZ = 360, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
end

event 20
      Hint = str[9]  -- "B"
  0:  Cmp  {"MapVar0", Value = 1,   jump = 7}
  1:  Set  {"MapVar0", Value = 1}
  2:  SetDoorState  {Id = 55, State = 1}
  3:  SetDoorState  {Id = 56, State = 1}
  4:  SetDoorState  {Id = 57, State = 0}
  5:  SetDoorState  {Id = 58, State = 0}
  6:  SetTexture  {Facet = 2011, Name = "t1swdu"}
  7:  Exit  {}
end

event 21
      Hint = str[10]  -- "C"
  0:  Cmp  {"MapVar1", Value = 1,   jump = 7}
  1:  Set  {"MapVar1", Value = 1}
  2:  SetDoorState  {Id = 57, State = 1}
  3:  SetDoorState  {Id = 58, State = 1}
  4:  SetDoorState  {Id = 55, State = 0}
  5:  SetDoorState  {Id = 56, State = 0}
  6:  SetTexture  {Facet = 2012, Name = "t1swdu"}
  7:  Exit  {}
end

event 22
      Hint = str[11]  -- "D"
  0:  Cmp  {"MapVar2", Value = 1,   jump = 8}
  1:  Set  {"MapVar2", Value = 1}
  2:  SetDoorState  {Id = 40, State = 1}
  3:  SetDoorState  {Id = 52, State = 1}
  4:  SetDoorState  {Id = 51, State = 1}
  5:  SetDoorState  {Id = 57, State = 0}
  6:  SetDoorState  {Id = 58, State = 0}
  7:  SetTexture  {Facet = 2013, Name = "t1swdu"}
  8:  Exit  {}
end

event 23
      Hint = str[12]  -- "E"
  0:  Cmp  {"MapVar3", Value = 1,   jump = 8}
  1:  Set  {"MapVar3", Value = 1}
  2:  SetDoorState  {Id = 57, State = 1}
  3:  SetDoorState  {Id = 58, State = 1}
  4:  SetDoorState  {Id = 40, State = 0}
  5:  SetDoorState  {Id = 52, State = 0}
  6:  SetDoorState  {Id = 51, State = 0}
  7:  SetTexture  {Facet = 2009, Name = "t1swdu"}
  8:  Exit  {}
end

event 24
      Hint = str[13]  -- "F"
  0:  Cmp  {"MapVar4", Value = 1,   jump = 8}
  1:  Set  {"MapVar4", Value = 1}
  2:  SetDoorState  {Id = 36, State = 1}
  3:  SetDoorState  {Id = 47, State = 1}
  4:  SetDoorState  {Id = 48, State = 1}
  5:  SetDoorState  {Id = 55, State = 0}
  6:  SetDoorState  {Id = 56, State = 0}
  7:  SetTexture  {Facet = 2008, Name = "t1swdu"}
  8:  Exit  {}
end

event 25
      Hint = str[14]  -- "G"
  0:  Cmp  {"MapVar5", Value = 1,   jump = 5}
  1:  Set  {"MapVar5", Value = 1}
  2:  SetDoorState  {Id = 59, State = 1}
  3:  SetDoorState  {Id = 60, State = 1}
  4:  SetTexture  {Facet = 2007, Name = "t1swdu"}
  5:  Exit  {}
end

event 26
      Hint = str[15]  -- "H"
  0:  Cmp  {"MapVar6", Value = 1,   jump = 8}
  1:  Set  {"MapVar6", Value = 1}
  2:  SetDoorState  {Id = 38, State = 1}
  3:  SetDoorState  {Id = 44, State = 1}
  4:  SetDoorState  {Id = 36, State = 0}
  5:  SetDoorState  {Id = 47, State = 0}
  6:  SetDoorState  {Id = 48, State = 0}
  7:  SetTexture  {Facet = 2006, Name = "t1swdu"}
  8:  Exit  {}
end

event 27
      Hint = str[16]  -- "I"
  0:  Cmp  {"MapVar7", Value = 1,   jump = 8}
  1:  Set  {"MapVar7", Value = 1}
  2:  SetDoorState  {Id = 36, State = 1}
  3:  SetDoorState  {Id = 47, State = 1}
  4:  SetDoorState  {Id = 48, State = 1}
  5:  SetDoorState  {Id = 38, State = 0}
  6:  SetDoorState  {Id = 44, State = 0}
  7:  SetTexture  {Facet = 2002, Name = "t1swdu"}
  8:  Exit  {}
end

event 28
      Hint = str[17]  -- "J"
  0:  Cmp  {"MapVar8", Value = 1,   jump = 9}
  1:  Set  {"MapVar8", Value = 1}
  2:  SetDoorState  {Id = 36, State = 1}
  3:  SetDoorState  {Id = 47, State = 1}
  4:  SetDoorState  {Id = 48, State = 1}
  5:  SetDoorState  {Id = 40, State = 0}
  6:  SetDoorState  {Id = 52, State = 0}
  7:  SetDoorState  {Id = 51, State = 0}
  8:  SetTexture  {Facet = 2003, Name = "t1swdu"}
  9:  Exit  {}
end

event 29
      Hint = str[18]  -- "K"
  0:  Cmp  {"MapVar9", Value = 1,   jump = 7}
  1:  Set  {"MapVar9", Value = 1}
  2:  SetDoorState  {Id = 59, State = 1}
  3:  SetDoorState  {Id = 60, State = 1}
  4:  SetDoorState  {Id = 57, State = 0}
  5:  SetDoorState  {Id = 58, State = 0}
  6:  SetTexture  {Facet = 2004, Name = "t1swdu"}
  7:  Exit  {}
end

event 30
      Hint = str[19]  -- "L"
  0:  Cmp  {"MapVar10", Value = 1,   jump = 7}
  1:  Set  {"MapVar10", Value = 1}
  2:  SetDoorState  {Id = 38, State = 1}
  3:  SetDoorState  {Id = 44, State = 1}
  4:  SetDoorState  {Id = 55, State = 0}
  5:  SetDoorState  {Id = 56, State = 0}
  6:  SetTexture  {Facet = 2005, Name = "t1swdu"}
  7:  Exit  {}
end

event 31
      Hint = str[20]  -- "M"
  0:  MoveToMap  {X = 9000, Y = 1916, Z = -767, Direction = 128, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 32
      Hint = str[21]  -- "N"
  0:  Cmp  {"MapVar11", Value = 1,   jump = 9}
  1:  Set  {"MapVar11", Value = 1}
  2:  SetDoorState  {Id = 40, State = 1}
  3:  SetDoorState  {Id = 51, State = 1}
  4:  SetDoorState  {Id = 52, State = 1}
  5:  SetDoorState  {Id = 36, State = 0}
  6:  SetDoorState  {Id = 47, State = 0}
  7:  SetDoorState  {Id = 48, State = 0}
  8:  SetTexture  {Facet = 2000, Name = "t1swdu"}
  9:  Exit  {}
end

event 33
      Hint = str[22]  -- "O"
  0:  Cmp  {"MapVar12", Value = 1,   jump = 7}
  1:  Set  {"MapVar12", Value = 1}
  2:  SetDoorState  {Id = 57, State = 1}
  3:  SetDoorState  {Id = 58, State = 1}
  4:  SetDoorState  {Id = 59, State = 0}
  5:  SetDoorState  {Id = 60, State = 0}
  6:  SetTexture  {Facet = 1999, Name = "t1swdu"}
  7:  Exit  {}
end

event 34
      Hint = str[23]  -- "P"
  0:  Set  {"MapVar0", Value = 0}
  1:  Set  {"MapVar1", Value = 0}
  2:  Set  {"MapVar2", Value = 0}
  3:  Set  {"MapVar3", Value = 0}
  4:  Set  {"MapVar4", Value = 0}
  5:  Set  {"MapVar5", Value = 0}
  6:  Set  {"MapVar6", Value = 0}
  7:  Set  {"MapVar7", Value = 0}
  8:  Set  {"MapVar8", Value = 0}
  9:  Set  {"MapVar9", Value = 0}
  10: Set  {"MapVar10", Value = 0}
  11: Set  {"MapVar11", Value = 0}
  12: Set  {"MapVar12", Value = 0}
  13: SetTexture  {Facet = 2011, Name = "T1swDd"}
  14: SetTexture  {Facet = 2012, Name = "T1swDd"}
  15: SetTexture  {Facet = 2013, Name = "T1swDd"}
  16: SetTexture  {Facet = 2009, Name = "T1swDd"}
  17: SetTexture  {Facet = 2008, Name = "T1swDd"}
  18: SetTexture  {Facet = 2007, Name = "T1swDd"}
  19: SetTexture  {Facet = 2006, Name = "T1swDd"}
  20: SetTexture  {Facet = 2002, Name = "T1swDd"}
  21: SetTexture  {Facet = 2003, Name = "T1swDd"}
  22: SetTexture  {Facet = 2004, Name = "T1swDd"}
  23: SetTexture  {Facet = 2005, Name = "T1swDd"}
  24: SetTexture  {Facet = 2000, Name = "T1swDd"}
  25: SetTexture  {Facet = 1999, Name = "T1swDd"}
  26: SetDoorState  {Id = 40, State = 0}
  27: SetDoorState  {Id = 51, State = 0}
  28: SetDoorState  {Id = 52, State = 0}
  29: SetDoorState  {Id = 36, State = 0}
  30: SetDoorState  {Id = 47, State = 0}
  31: SetDoorState  {Id = 48, State = 0}
  32: SetDoorState  {Id = 38, State = 0}
  33: SetDoorState  {Id = 44, State = 0}
  34: SetDoorState  {Id = 55, State = 0}
  35: SetDoorState  {Id = 56, State = 0}
  36: SetDoorState  {Id = 57, State = 0}
  37: SetDoorState  {Id = 58, State = 0}
  38: SetDoorState  {Id = 59, State = 0}
  39: SetDoorState  {Id = 60, State = 0}
end

event 41
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 1}
end

event 42
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 2}
end

event 43
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 3}
end

event 44
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 4}
end

event 45
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 5}
end

event 46
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 6}
end

event 47
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 7}
  1:  Cmp  {"MapVar19", Value = 1,   jump = 5}
  2:  Set  {"MapVar19", Value = 1}
  3:  SummonObject  {Type = 2100, X = 9856, Y = 4992, Z = -1024, Speed = 512, Count = 3, RandomAngle = false}         -- starburst
  4:  SummonMonsters  {TypeIndexInMapStats = 2, Level = 2, Count = 3, X = 9856, Y = 4992, Z = -1024}
  5:  Exit  {}
end

event 48
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 8}
end

event 49
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 9}
end

event 50
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 10}
end

event 51
      Hint = str[1]  -- "Exit Door"
  0:  MoveToMap  {X = -18400, Y = -14982, Z = 1600, Direction = 512, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 8, Name = "OutE3.Odm"}
end

event 55
  0:  Cmp  {"MapVar24", Value = 1,   jump = 3}
  1:  Set  {"MapVar24", Value = 1}
  2:  GiveItem  {Strength = 4, Type = const.ItemType.Armor_, Id = 0}
  3:  Exit  {}
end

event 60
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 45, State = 1}
  1:  SetDoorState  {Id = 46, State = 1}
end

event 61
      Hint = str[7]  -- "Door"
  0:  SetDoorState  {Id = 23, State = 1}
end

event 62
      Hint = str[3]  -- "Switch"
  0:  SetDoorState  {Id = 31, State = 1}
  1:  SetDoorState  {Id = 32, State = 1}
end

event 63
  0:  SetDoorState  {Id = 30, State = 1}
end

event 64
  0:  Cmp  {"MapVar13", Value = 1,   jump = 3}
  1:  CastSpell  {Spell = 12, Mastery = const.Novice, Skill = 15, FromX = 0, FromY = 0, FromZ = 0, ToX = 0, ToY = 0, ToZ = 0}         -- "Wizard Eye"
  2:  Set  {"MapVar13", Value = 1}
  3:  Exit  {}
end

event 65
  0:  OnTimer  {Second = 1}
  1:  Set  {"MapVar13", Value = 0}
end

event 66
  0:  OnLoadMap  {}
  1:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  2:  GoTo  {jump = 4}

  3:  SetTexture  {Facet = 2011, Name = "t1swdu"}
  4:  Cmp  {"MapVar1", Value = 1,   jump = 6}
  5:  GoTo  {jump = 7}

  6:  SetTexture  {Facet = 2012, Name = "t1swdu"}
  7:  Cmp  {"MapVar2", Value = 1,   jump = 9}
  8:  GoTo  {jump = 10}

  9:  SetTexture  {Facet = 2013, Name = "t1swdu"}
  10: Cmp  {"MapVar2", Value = 1,   jump = 12}
  11: GoTo  {jump = 13}

  12: SetTexture  {Facet = 2009, Name = "t1swdu"}
  13: Cmp  {"MapVar4", Value = 1,   jump = 15}
  14: GoTo  {jump = 16}

  15: SetTexture  {Facet = 2008, Name = "t1swdu"}
  16: Cmp  {"MapVar5", Value = 1,   jump = 18}
  17: GoTo  {jump = 19}

  18: SetTexture  {Facet = 2007, Name = "t1swdu"}
  19: Cmp  {"MapVar6", Value = 1,   jump = 21}
  20: GoTo  {jump = 22}

  21: SetTexture  {Facet = 2006, Name = "t1swdu"}
  22: Cmp  {"MapVar7", Value = 1,   jump = 24}
  23: GoTo  {jump = 25}

  24: SetTexture  {Facet = 2002, Name = "t1swdu"}
  25: Cmp  {"MapVar8", Value = 1,   jump = 27}
  26: GoTo  {jump = 28}

  27: SetTexture  {Facet = 2003, Name = "t1swdu"}
  28: Cmp  {"MapVar9", Value = 1,   jump = 30}
  29: GoTo  {jump = 31}

  30: SetTexture  {Facet = 2004, Name = "t1swdu"}
  31: Cmp  {"MapVar10", Value = 1,   jump = 33}
  32: GoTo  {jump = 34}

  33: SetTexture  {Facet = 2005, Name = "t1swdu"}
  34: Cmp  {"MapVar11", Value = 1,   jump = 36}
  35: GoTo  {jump = 37}

  36: SetTexture  {Facet = 2000, Name = "t1swdu"}
  37: Cmp  {"MapVar12", Value = 1,   jump = 39}
  38: Exit  {}

  39: SetTexture  {Facet = 1999, Name = "t1swdu"}
end
