str[0] = " "
str[1] = "Door"
str[2] = "Lever"
str[3] = "Chest"
str[4] = "Cell"
str[5] = "The door is locked"
str[6] = "Temple of Tsantsa"
str[7] = ""


event 1
      Hint = str[1]  -- "Door"
      MazeInfo = str[6]  -- "Temple of Tsantsa"
  0:  SetDoorState  {Id = 1, State = 1}
end

event 2
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 2, State = 1}
end

event 3
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 3, State = 1}
end

event 4
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 4, State = 1}
end

event 5
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 5, State = 1}
end

event 6
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 6, State = 1}
end

event 7
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 7, State = 1}
end

event 8
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 8, State = 1}
end

event 9
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 9, State = 1}
end

event 10
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 10, State = 1}
end

event 11
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 11, State = 1}
end

event 12
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 1}
end

event 13
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 2}
end

event 31
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 3}
end

event 14
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 4}
end

event 15
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 5}
end

event 16
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 6}
end

event 17
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 12, State = 2}         -- switch state
  1:  SetDoorState  {Id = 13, State = 2}         -- switch state
end

event 18
  0:  Set  {"MapVar2", Value = 1}
end

event 19
      Hint = str[4]  -- "Cell"
  0:  Cmp  {"Inventory", Value = 566,   jump = 3}         -- "Cell Key"
  1:  StatusText  {Str = 5}         -- "The door is locked"
  2:  Exit  {}

  3:  Subtract  {"Inventory", Value = 566}         -- "Cell Key"
  4:  SetDoorState  {Id = 25, State = 1}
end

event 20
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 14, State = 1}
  1:  Add  {"MapVar0", Value = 1}
  2:  Cmp  {"MapVar0", Value = 2,   jump = 4}
  3:  Exit  {}

  4:  SetDoorState  {Id = 16, State = 1}
end

event 21
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 15, State = 1}
  1:  Add  {"MapVar0", Value = 1}
  2:  Cmp  {"MapVar0", Value = 2,   jump = 4}
  3:  Exit  {}

  4:  SetDoorState  {Id = 16, State = 1}
end

event 23
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 18, State = 2}         -- switch state
  1:  SetDoorState  {Id = 19, State = 2}         -- switch state
end

event 24
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 20, State = 2}         -- switch state
  1:  SetDoorState  {Id = 21, State = 2}         -- switch state
end

event 26
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 22, State = 2}         -- switch state
  1:  SetDoorState  {Id = 23, State = 2}         -- switch state
end

event 25
  0:  SetDoorState  {Id = 17, State = 1}
end

event 27
  0:  Set  {"MapVar2", Value = 0}
end

event 28
  0:  OnTimer  {IntervalInHalfMinutes = 2}
  1:  Cmp  {"MapVar2", Value = 1,   jump = 3}
  2:  Exit  {}

  3:  DamagePlayer  {Player = "All", DamageType = const.Damage.Magic, Damage = 5}
end

event 29
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 24, State = 1}
end

event 30
  0:  Cmp  {"QBits", Value = 6,   jump = 4}         --  6 T3, given when you rescue prisoner
  1:  SpeakNPC  {NPC = 155}         -- "Sherell Ivanaveh"
  2:  Set  {"NPCs", Value = 155}         -- "Sherell Ivanaveh"
  3:  Set  {"QBits", Value = 6}         --  6 T3, given when you rescue prisoner
  4:  Exit  {}
end

event 50
  0:  MoveToMap  {X = 7474, Y = 17670, Z = 97, Direction = 1024, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 6, Name = "OutD2.Odm"}
end

event 55
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  Set  {"MapVar0", Value = 1}
  2:  GiveItem  {Strength = 5, Type = const.ItemType.Spear, Id = 0}
  3:  Exit  {}
end
