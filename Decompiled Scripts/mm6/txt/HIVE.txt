str[0] = " "
str[1] = "Door"
str[2] = "Chest"
str[3] = "The Door Won't Budge"
str[4] = "Warning!  Flush system activated!"
str[5] = "Flush sequence in progress."
str[6] = "Flush system activated."
str[7] = "Flush sequence complete."
str[8] = "Flush system deactivated"
str[9] = "Switch"
str[10] = "empty"
str[11] = "empty"
str[12] = "The Hive"
str[13] = "Poison spray!"
str[14] = "empty"
str[15] = "Protected"
str[16] = "empty"
str[17] = "Keg"
str[18] = " +20 Hit Points"
str[19] = "empty"
str[20] = "empty"
str[21] = "empty"
str[22] = "empty"
str[23] = "empty"
str[24] = "empty"
str[25] = "The door is locked"
str[26] = "empty"
str[27] = "Exit"
str[28] = ""


event 1
      Hint = str[1]  -- "Door"
      MazeInfo = str[12]  -- "The Hive"
  0:  SetDoorState  {Id = 1, State = 1}
end

event 2
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 2, State = 1}
end

event 3
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 3, State = 1}
end

event 4
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 4, State = 1}
end

event 5
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 5, State = 1}
end

event 6
      Hint = str[1]  -- "Door"
  0:  Cmp  {"Inventory", Value = 570,   jump = 3}         -- "Hive Sanctum Key"
  1:  StatusText  {Str = 25}         -- "The door is locked"
  2:  Exit  {}

  3:  SetDoorState  {Id = 6, State = 1}
  4:  Subtract  {"Inventory", Value = 570}         -- "Hive Sanctum Key"
end

event 7
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 7, State = 1}
end

event 8
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 8, State = 1}
end

event 9
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 9, State = 1}
end

event 10
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 10, State = 1}
end

event 11
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 11, State = 1}
end

event 12
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 12, State = 1}
end

event 13
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 13, State = 1}
end

event 14
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 14, State = 1}
end

event 16
      Hint = str[9]  -- "Switch"
  0:  MoveToMap  {X = 468, Y = 3690, Z = 1, Direction = 45, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  1:  SetDoorState  {Id = 16, State = 2}         -- switch state
  2:  Set  {"MapVar0", Value = 0}
  3:  StatusText  {Str = 8}         -- "Flush system deactivated"
end

event 17
  0:  Cmp  {"MapVar21", Value = 1,   jump = 4}
  1:  Cmp  {"MapVar0", Value = 1,   jump = 4}
  2:  StatusText  {Str = 6}         -- "Flush system activated."
  3:  Set  {"MapVar0", Value = 0}
  4:  Exit  {}
end

event 19
  0:  Cmp  {"MapVar21", Value = 1,   jump = 3}
  1:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  2:  Set  {"MapVar0", Value = 1}
  3:  Exit  {}
end

event 21
      Hint = str[1]  -- "Door"
  0:  StatusText  {Str = 3}         -- "The Door Won't Budge"
end

event 22
  0:  Set  {"MapVar3", Value = 0}
end

event 23
      Hint = str[17]  -- "Keg"
  0:  StatusText  {Str = 18}         -- " +20 Hit Points"
  1:  Add  {"HP", Value = 20}
end

event 27
  0:  SetDoorState  {Id = 43, State = 1}
end

event 28
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 28, State = 1}
end

event 29
  0:  Set  {"MapVar0", Value = 0}
end

event 31
      Hint = str[9]  -- "Switch"
  0:  SetDoorState  {Id = 31, State = 1}
  1:  SetDoorState  {Id = 32, State = 1}
end

event 33
  0:  SetDoorState  {Id = 35, State = 2}         -- switch state
  1:  SetDoorState  {Id = 36, State = 2}         -- switch state
end

event 34
      Hint = str[9]  -- "Switch"
  0:  SetDoorState  {Id = 37, State = 2}         -- switch state
  1:  SetDoorState  {Id = 38, State = 2}         -- switch state
end

event 36
      Hint = str[9]  -- "Switch"
  0:  SetDoorState  {Id = 41, State = 2}         -- switch state
  1:  SetDoorState  {Id = 43, State = 2}         -- switch state
end

event 38
  0:  SetDoorState  {Id = 42, State = 1}
end

event 39
  0:  SetDoorState  {Id = 45, State = 1}
  1:  SetDoorState  {Id = 46, State = 1}
end

event 41
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 1}
end

event 42
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 2}
  1:  Set  {"MapVar10", Value = 1}
end

event 43
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 3}
end

event 44
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 4}
end

event 45
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 5}
end

event 48
      Hint = str[27]  -- "Exit"
  0:  MoveToMap  {X = -14355, Y = 15010, Z = 3841, Direction = 1408, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 6, Name = "OutA1.Odm"}
end

event 50
      Hint = str[9]  -- "Switch"
  0:  SetDoorState  {Id = 50, State = 1}
  1:  Set  {"MapVar21", Value = 1}
end

event 51
  0:  OnTimer  {IntervalInHalfMinutes = 5}
  1:  Cmp  {"MapVar20", Value = 1,   jump = 3}
  2:  Exit  {}

  3:  StatusText  {Str = 7}         -- "Flush sequence complete."
  4:  Set  {"MapVar20", Value = 0}
end

event 54
  0:  OnTimer  {IntervalInHalfMinutes = 10}
  1:  Cmp  {"MapVar19", Value = 1,   jump = 3}
  2:  Exit  {}

  3:  StatusText  {Str = 5}         -- "Flush sequence in progress."
  4:  Cmp  {"MapVar1", Value = 1,   jump = 6}
  5:  DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  6:  Set  {"MapVar20", Value = 1}
  7:  Set  {"MapVar19", Value = 0}
  8:  Exit  {}
end

event 56
      Hint = str[15]  -- "Protected"
  0:  Set  {"MapVar1", Value = 1}
end

event 57
  0:  OnTimer  {IntervalInHalfMinutes = 120}
  1:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  2:  Exit  {}

  3:  StatusText  {Str = 4}         -- "Warning!  Flush system activated!"
  4:  Set  {"MapVar19", Value = 1}
end

event 58
  0:  Set  {"MapVar1", Value = 0}
end

event 60
      Hint = str[27]  -- "Exit"
  0:  Cmp  {"QBits", Value = 202,   jump = 3}         -- NPC
  1:  StatusText  {Str = 25}         -- "The door is locked"
  2:  Exit  {}

  3:  ForPlayer  ("All")
  4:  Cmp  {"Inventory", Value = 544,   jump = 7}         -- "Ritual of the Void"
  5:  EnterHouse  {Id = 601}         -- Lose
  6:  Exit  {}

  7:  Set  {"QBits", Value = 237}         -- NPC
  8:  Subtract  {"QBits", Value = 198}         -- Quest item bits for seer
  9:  Subtract  {"Inventory", Value = 544}         -- "Ritual of the Void"
  10: Set  {"Awards", Value = 36}         -- "Destroyed the Hive and Saved Enroth"
  11: EnterHouse  {Id = 600}         -- Win
end

event 100
  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 180,   jump = 3}         -- NPC
  2:  Exit  {}

  3:  SetDoorState  {Id = 28, State = 0}
  4:  SetDoorState  {Id = 30, State = 1}
  5:  SetDoorState  {Id = 51, State = 0}
  6:  SetDoorState  {Id = 52, State = 0}
  7:  SetDoorState  {Id = 53, State = 1}
end
