str[0] = " "
str[1] = "Door"
str[2] = "Switch"
str[3] = "Chest"
str[4] = "This Door won't budge."
str[5] = "Wall"
str[6] = "Exit"
str[7] = "Luftka's Estate"
str[8] = ""


event 1
      Hint = str[1]  -- "Door"
      MazeInfo = str[7]  -- "Luftka's Estate"
  0:  SetDoorState  {Id = 1, State = 1}
end

event 2
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 2, State = 1}
end

event 3
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 3, State = 1}
end

event 5
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 5, State = 1}
end

event 6
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 6, State = 1}
end

event 7
  0:  SetDoorState  {Id = 7, State = 1}
end

event 8
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 8, State = 1}
end

event 9
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 9, State = 1}
  1:  Set  {"MapVar1", Value = 1}
end

event 10
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 10, State = 1}
end

event 11
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 11, State = 1}
end

event 12
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 12, State = 1}
end

event 14
      Hint = str[2]  -- "Switch"
  0:  SetDoorState  {Id = 14, State = 2}         -- switch state
  1:  SetDoorState  {Id = 15, State = 2}         -- switch state
end

event 16
      Hint = str[2]  -- "Switch"
  0:  SetDoorState  {Id = 16, State = 2}         -- switch state
  1:  SetDoorState  {Id = 4, State = 2}         -- switch state
end

event 17
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 17, State = 1}
end

event 18
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 18, State = 1}
end

event 19
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 19, State = 1}
end

event 20
      Hint = str[2]  -- "Switch"
  0:  SetDoorState  {Id = 20, State = 2}         -- switch state
  1:  SetDoorState  {Id = 13, State = 2}         -- switch state
end

event 21
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 21, State = 1}
end

event 22
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 22, State = 1}
end

event 23
      Hint = str[5]  -- "Wall"
  0:  SetDoorState  {Id = 23, State = 1}
end

event 24
      Hint = str[24]
  0:  SetDoorState  {Id = 24, State = 1}
end

event 25
      Hint = str[5]  -- "Wall"
  0:  SetDoorState  {Id = 25, State = 1}
end

event 26
      Hint = str[2]  -- "Switch"
  0:  SetDoorState  {Id = 26, State = 2}         -- switch state
  1:  SetDoorState  {Id = 27, State = 2}         -- switch state
end

event 28
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 28, State = 1}
end

event 29
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 29, State = 1}
end

event 30
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 30, State = 1}
end

event 31
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 31, State = 1}
end

event 32
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 32, State = 1}
end

event 33
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 33, State = 1}
end

event 34
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 34, State = 1}
end

event 35
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 35, State = 1}
end

event 42
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 1}
end

event 43
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 2}
end

event 44
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 3}
end

event 45
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 4}
end

event 46
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 5}
end

event 47
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 6}
end

event 48
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 7}
end

event 49
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 8}
end

event 50
      Hint = str[3]  -- "Chest"
  0:  Cmp  {"MapVar0", Value = 1,   jump = 2}
  1:  Cmp  {"QBits", Value = 2,   jump = 7}         --  2 D11, given for glass shard for staff of terrax quest
  2:  OpenChest  {Id = 9}
  3:  Set  {"MapVar0", Value = 1}
  4:  Set  {"QBits", Value = 2}         --  2 D11, given for glass shard for staff of terrax quest
  5:  Set  {"QBits", Value = 186}         -- Quest item bits for seer
  6:  Exit  {}

  7:  OpenChest  {Id = 8}
end

event 51
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 10}
end

event 52
      Hint = str[1]  -- "Door"
  0:  Exit  {}
end

event 53
      Hint = str[1]  -- "Door"
  0:  StatusText  {Str = 4}         -- "This Door won't budge."
end

event 60
      Hint = str[6]  -- "Exit"
  0:  MoveToMap  {X = -19677, Y = -17439, Z = 96, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 6, Name = "OutD3.Odm"}
end

event 65
  0:  MoveToMap  {X = -5768, Y = 656, Z = -1296, Direction = 1536, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 66
  0:  MoveToMap  {X = -6416, Y = 7560, Z = -1296, Direction = 512, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 69
  0:  MoveToMap  {X = 6538, Y = 114, Z = -255, Direction = 1536, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 70
  0:  MoveToMap  {X = 6537, Y = 5945, Z = -255, Direction = 512, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 71
  0:  Cmp  {"MapVar1", Value = 1,   jump = 2}
  1:  Exit  {}

  2:  MoveToMap  {X = 3584, Y = -5248, Z = -832, Direction = 512, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  3:  Subtract  {"MapVar1", Value = 1}
  4:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 1, Count = 1, X = 1408, Y = -4992, Z = -832}
  5:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 1, X = 1408, Y = -4864, Z = -832}
  6:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 1, X = 1408, Y = -4736, Z = -832}
end

event 75
  0:  OpenChest  {Id = 0}
end
