str[0] = " "
str[1] = "Door"
str[2] = "Chest"
str[3] = "Chest"
str[4] = "Cabinet"
str[5] = "The door is locked"
str[6] = "Exit"
str[7] = "Temple of Babzot"
str[8] = "Gepetto's Thermos"
str[9] = "The caudron is empty,"
str[10] = ""


event 1
      MazeInfo = str[7]  -- "Temple of Babzot"
  0:  SetDoorState  {Id = 1, State = 1}
end

event 2
  0:  SetDoorState  {Id = 2, State = 1}
end

event 3
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 3, State = 1}
end

event 4
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 0}
end

event 5
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 1}
end

event 6
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 2}
end

event 7
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 3}
end

event 8
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 4}
end

event 9
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 5}
end

event 10
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 6}
end

event 11
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 7}
end

event 12
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 8}
end

event 13
      Hint = str[4]  -- "Cabinet"
  0:  GoTo  {jump = 6}

  1:  Cmp  {"QBits", Value = 51,   jump = 5}         -- 51 T4, Given when characters find Silver Chalice.
  2:  Set  {"QBits", Value = 51}         -- 51 T4, Given when characters find Silver Chalice.
  3:  Add  {"Inventory", Value = 434}         -- "Sacred Chalice"
  4:  Set  {"QBits", Value = 188}         -- Quest item bits for seer
  5:  Exit  {}

  6:  Cmp  {"MapVar4", Value = 1,   jump = 10}
  7:  GiveItem  {Strength = 4, Type = const.ItemType.Sword, Id = 0}
  8:  GiveItem  {Strength = 4, Type = const.ItemType.Armor_, Id = 0}
  9:  GiveItem  {Strength = 4, Type = const.ItemType.Shield_, Id = 0}
  10: Set  {"MapVar4", Value = 1}
  10: Exit  {}
end

event 14
      Hint = str[6]  -- "Exit"
  0:  MoveToMap  {X = -21468, Y = -263, Z = 193, Direction = 1536, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "OutE3.odm"}
end

event 15
  0:  OnTimer  {Hour = 21}
  1:  SetLight  {Id = 0, On = false}
  2:  SetLight  {Id = 1, On = false}
  3:  SetTexture  {Facet = 79, Name = "SKY_NIT1"}
end

event 16
  0:  OnTimer  {Hour = 88}
  1:  SetLight  {Id = 0, On = true}
  2:  SetLight  {Id = 1, On = true}
  3:  SetTexture  {Facet = 79, Name = "OrAir256"}
end

event 17
  0:  OnTimer  {Hour = 208}
  1:  SetLight  {Id = 0, On = true}
  2:  SetLight  {Id = 1, On = false}
  3:  SetTexture  {Facet = 79, Name = "SKY_SNS1"}
end

event 18
  0:  OnTimer  {Hour = 244}
  1:  SetLight  {Id = 0, On = true}
  2:  SetLight  {Id = 1, On = false}
  3:  SetTexture  {Facet = 79, Name = "SKY_SNS1"}
end

event 19
  0:  OnLoadMap  {}
  1:  Cmp  {"HourIs", Value = 2100,   jump = 5}
  2:  Cmp  {"HourIs", Value = 2000,   jump = 9}
  3:  Cmp  {"HourIs", Value = 600,   jump = 13}
  4:  Cmp  {"HourIs", Value = 500,   jump = 9}
  5:  SetLight  {Id = 0, On = false}
  6:  SetLight  {Id = 1, On = false}
  7:  SetTexture  {Facet = 79, Name = "SKY_NIT1"}
  8:  Exit  {}

  9:  SetLight  {Id = 0, On = true}
  10: SetLight  {Id = 1, On = false}
  11: SetTexture  {Facet = 79, Name = "SKY_SNS1"}
  12: Exit  {}

  13: Exit  {}
end

event 20
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  Set  {"MapVar0", Value = 1}
  2:  Add  {"Inventory", Value = 286}         -- "Sun Ray"
  3:  Exit  {}
end

event 21
  0:  Cmp  {"MapVar1", Value = 1,   jump = 3}
  1:  Set  {"MapVar1", Value = 1}
  2:  Add  {"Inventory", Value = 286}         -- "Sun Ray"
  3:  Exit  {}
end

event 22
  0:  Cmp  {"MapVar2", Value = 1,   jump = 3}
  1:  Set  {"MapVar2", Value = 1}
  2:  Add  {"Inventory", Value = 286}         -- "Sun Ray"
  3:  Exit  {}
end

event 23
  0:  Cmp  {"MapVar3", Value = 1,   jump = 3}
  1:  Set  {"MapVar3", Value = 1}
  2:  Add  {"Inventory", Value = 286}         -- "Sun Ray"
  3:  Exit  {}
end

event 24
  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 481,   jump = 6}         -- Abandoned Temple Reload Once
  2:  Set  {"QBits", Value = 481}         -- Abandoned Temple Reload Once
  3:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 1, X = -1120, Y = 596, Z = -63}
  4:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 2, X = 1599, Y = 187, Z = 1}
  5:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 1, X = -1120, Y = 580, Z = -63}
  6:  Exit  {}
end

event 25
      Hint = str[8]  -- "Gepetto's Thermos"
  0:  Cmp  {"QBits", Value = 483,   jump = 5}         -- Gepeto's Thermos
  1:  ForPlayer  ("All")
  2:  Set  {"RepairSkill", Value = 68}
  3:  Set  {"QBits", Value = 483}         -- Gepeto's Thermos
  4:  Exit  {}

  5:  StatusText  {Str = 9}         -- "The caudron is empty,"
end
