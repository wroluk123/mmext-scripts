str[0] = " "
str[1] = "Switch"
str[2] = "Chest"
str[3] = "Chest"
str[4] = "Exit"
str[5] = "You easily defeat the lock."
str[6] = "A teleporter!"
str[7] = "You are unable to pick the lock."
str[8] = "-50 Speed Temporary"
str[9] = "Cage"
str[10] = "Haunt of the Rusalka"
str[11] = ""


event 1
      Hint = str[1]  -- "Switch"
      MazeInfo = str[10]  -- "Haunt of the Rusalka"
  0:  SetDoorState  {Id = 1, State = 1}
  1:  SetTexture  {Facet = 635, Name = "t1swbu"}
  2:  Set  {"MapVar2", Value = 1}
end

event 2
  0:  SetDoorState  {Id = 2, State = 1}
end

event 3
      Hint = str[1]  -- "Switch"
  0:  SetDoorState  {Id = 3, State = 2}         -- switch state
  1:  Set  {"MapVar1", Value = 1}
  2:  SetTexture  {Facet = 643, Name = "t1swbu"}
  3:  SetTexture  {Facet = 639, Name = "t1swbu"}
end

event 4
      Hint = str[1]  -- "Switch"
  0:  SetDoorState  {Id = 4, State = 2}         -- switch state
  1:  SetTexture  {Facet = 647, Name = "t1swbu"}
  2:  Set  {"MapVar3", Value = 1}
end

event 5
  0:  SetDoorState  {Id = 1, State = 0}
  1:  SetTexture  {Facet = 635, Name = "t1swbd"}
  2:  Set  {"MapVar4", Value = 1}
end

event 6
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 1}
end

event 7
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 2}
end

event 8
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 3}
end

event 9
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 4}
end

event 10
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 5}
end

event 11
      Hint = str[2]  -- "Chest"
  0:  Cmp  {"MapVar5", Value = 1,   jump = 3}
  1:  DamagePlayer  {Player = "All", DamageType = const.Damage.Magic, Damage = 200}
  2:  Set  {"MapVar5", Value = 1}
  3:  OpenChest  {Id = 6}
end

event 12
      Hint = str[2]  -- "Chest"
  0:  Cmp  {"MapVar6", Value = 1,   jump = 3}
  1:  DamagePlayer  {Player = "All", DamageType = const.Damage.Fire, Damage = 200}
  2:  Set  {"MapVar6", Value = 1}
  3:  OpenChest  {Id = 7}
end

event 13
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 8}
end

event 14
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 9}
end

event 16
  0:  DamagePlayer  {Player = "Random", DamageType = const.Damage.Fire, Damage = 40}
end

event 17
  0:  DamagePlayer  {Player = "Random", DamageType = const.Damage.Magic, Damage = 40}
end

event 18
  0:  DamagePlayer  {Player = "Random", DamageType = const.Damage.Magic, Damage = 50}
end

event 19
  0:  DamagePlayer  {Player = "Random", DamageType = const.Damage.Elec, Damage = 50}
end

event 20
  0:  DamagePlayer  {Player = "Random", DamageType = const.Damage.Fire, Damage = 50}
end

event 21
  0:  StatusText  {Str = 6}         -- "A teleporter!"
  1:  MoveToMap  {X = 3264, Y = -1336, Z = 513, Direction = 192, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 22
  0:  OnTimer  {IntervalInHalfMinutes = 5}
  1:  Cmp  {"MapVar1", Value = 1,   jump = 3}
  2:  Exit  {}

  3:  SetTexture  {Facet = 643, Name = "t1swbd"}
  4:  SetTexture  {Facet = 639, Name = "t1swbd"}
end

event 23
  0:  OnLoadMap  {}
  1:  Cmp  {"MapVar2", Value = 1,   jump = 3}
  2:  GoTo  {jump = 4}

  3:  SetTexture  {Facet = 635, Name = "t1swbu"}
  4:  Cmp  {"MapVar3", Value = 1,   jump = 6}
  5:  GoTo  {jump = 7}

  6:  SetTexture  {Facet = 647, Name = "t1swbu"}
  7:  Cmp  {"MapVar4", Value = 1,   jump = 9}
  8:  Exit  {}

  9:  SetTexture  {Facet = 635, Name = "t1swbd"}
end

event 24
  0:  OnTimer  {IntervalInHalfMinutes = 5}
  1:  Cmp  {"MapVar3", Value = 1,   jump = 3}
  2:  CastSpell  {Spell = 32, Mastery = const.Novice, Skill = 7, FromX = -3306, FromY = -1285, FromZ = 640, ToX = -2000, ToY = -1285, ToZ = 640}         -- "Ice Blast"
  3:  Exit  {}
end

event 25
      Hint = str[9]  -- "Cage"
  0:  Cmp  {"QBits", Value = 203,   jump = 11}         -- NPC
  1:  Set  {"QBits", Value = 203}         -- NPC
  2:  SpeakNPC  {NPC = 108}         -- "Drago Dazhbog"
  3:  MoveNPC  {NPC = 10, HouseId = 249}         -- "Jarek Nitsch" -> "House"
  4:  MoveNPC  {NPC = 338, HouseId = 494}         -- "Perun Mahike" -> "House"
  5:  MoveNPC  {NPC = 322, HouseId = 259}         -- "Stile Bloke" -> "House"
  6:  MoveNPC  {NPC = 181, HouseId = 248}         -- "Bronwyn Meck" -> "House"
  7:  MoveNPC  {NPC = 340, HouseId = 496}         -- "Leo Grant" -> "House"
  8:  MoveNPC  {NPC = 342, HouseId = 498}         -- "David Pike" -> "House"
  9:  MoveNPC  {NPC = 341, HouseId = 497}         -- "Boian Kowall" -> "House"
  10: MoveNPC  {NPC = 89, HouseId = 252}         -- "Errol Ostermann" -> "House"
  11: Exit  {}
end

event 50
      Hint = str[4]  -- "Exit"
  0:  MoveToMap  {X = 9230, Y = 7102, Z = 64, Direction = 512, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 6, Name = "OutB2.Odm"}
end
