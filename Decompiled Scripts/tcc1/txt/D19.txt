str[0] = " "
str[1] = "Switch"
str[2] = "Elevator platform"
str[3] = "Chest"
str[4] = "Door"
str[5] = "Pushing this switch gives you a bad feeling."
str[6] = "Uh oh...."
str[7] = "Here we go again�."
str[8] = "The switch doesn�t seem to work.."
str[9] = "Exit"
str[10] = "Chandelier"
str[11] = "Cauldron"
str[12] = "+50 Intellect permanent."
str[13] = "The Garrison"
str[14] = ""


event 2
      Hint = str[1]  -- "Switch"
      MazeInfo = str[13]  -- "The Garrison"
  0:  SetDoorState  {Id = 2, State = 0}
  1:  SetDoorState  {Id = 16, State = 2}         -- switch state
end

event 3
      Hint = str[1]  -- "Switch"
  0:  SetDoorState  {Id = 3, State = 0}
  1:  SetDoorState  {Id = 15, State = 2}         -- switch state
end

event 4
      Hint = str[1]  -- "Switch"
  0:  Set  {"MapVar1", Value = 1}
  1:  SetDoorState  {Id = 4, State = 1}
  2:  SetDoorState  {Id = 5, State = 1}
  3:  SetDoorState  {Id = 6, State = 1}
  4:  FaceExpression  {Player = "All", Frame = 39}
  5:  Cmp  {"MapVar0", Value = 1,   jump = 7}
  6:  Exit  {}

  7:  SetDoorState  {Id = 18, State = 1}
end

event 7
      Hint = str[1]  -- "Switch"
  0:  Set  {"MapVar0", Value = 1}
  1:  SetDoorState  {Id = 7, State = 1}
  2:  SetDoorState  {Id = 8, State = 1}
  3:  SetDoorState  {Id = 9, State = 1}
  4:  FaceExpression  {Player = "All", Frame = 39}
  5:  Cmp  {"MapVar1", Value = 1,   jump = 7}
  6:  Exit  {}

  7:  SetDoorState  {Id = 18, State = 1}
end

event 10
      Hint = str[1]  -- "Switch"
  0:  SetDoorState  {Id = 10, State = 1}
  1:  SetDoorState  {Id = 11, State = 1}
  2:  SetDoorState  {Id = 12, State = 1}
  3:  FaceExpression  {Player = "All", Frame = 48}
end

event 13
  0:  Cmp  {"MapVar19", Value = 1,   jump = 4}
  1:  StatusText  {Str = 6}         -- "Uh oh...."
  2:  SetDoorState  {Id = 13, State = 1}
  3:  Add  {"MapVar19", Value = 1}
  4:  Exit  {}
end

event 14
      Hint = str[4]  -- "Door"
  0:  SetDoorState  {Id = 14, State = 2}         -- switch state
end

event 15
      Hint = str[1]  -- "Switch"
  0:  SetDoorState  {Id = 15, State = 1}
  1:  SetDoorState  {Id = 3, State = 1}
end

event 16
  0:  SetDoorState  {Id = 16, State = 1}
end

event 17
  0:  Exit  {}
end

event 18
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 0}
end

event 19
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 1}
end

event 20
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 0}
end

event 21
  0:  OnTimer  {IntervalInHalfMinutes = 5}
  1:  CastSpell  {Spell = 32, Mastery = const.Master, Skill = 7, FromX = -2048, FromY = 9712, FromZ = -2282, ToX = -2048, ToY = 9050, ToZ = -2282}         -- "Ice Blast"
end

event 25
      Hint = str[10]  -- "Chandelier"
  0:  Cmp  {"MapVar4", Value = 1,   jump = 3}
  1:  Set  {"MapVar4", Value = 1}
  2:  Add  {"Inventory", Value = 436}         -- "Diamond"
  3:  Exit  {}
end

event 26
      Hint = str[10]  -- "Chandelier"
  0:  Cmp  {"MapVar5", Value = 1,   jump = 3}
  1:  Set  {"MapVar5", Value = 1}
  2:  Add  {"Inventory", Value = 436}         -- "Diamond"
  3:  Exit  {}
end

event 27
      Hint = str[10]  -- "Chandelier"
  0:  Cmp  {"MapVar6", Value = 1,   jump = 3}
  1:  Set  {"MapVar6", Value = 1}
  2:  Add  {"Inventory", Value = 436}         -- "Diamond"
  3:  Exit  {}
end

event 30
  0:  SetDoorState  {Id = 16, State = 0}
  1:  SetDoorState  {Id = 2, State = 0}
end

event 31
      Hint = str[1]  -- "Switch"
  0:  SetDoorState  {Id = 15, State = 0}
  1:  SetDoorState  {Id = 3, State = 0}
end

event 35
      Hint = str[11]  -- "Cauldron"
  0:  ForPlayer  ("All")
  1:  Cmp  {"QBits", Value = 464,   jump = 4}         -- Garrison Cauldron Once
  2:  Add  {"Experience", Value = 35000}
  3:  Set  {"QBits", Value = 464}         -- Garrison Cauldron Once
  4:  Exit  {}
end

event 50
      Hint = str[9]  -- "Exit"
  0:  MoveToMap  {X = 7762, Y = 16306, Z = 449, Direction = 1664, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 6, Name = "OutB1.Odm"}
end

event 90
  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 465,   jump = 9}         -- The Garrison Once
  2:  Set  {"QBits", Value = 465}         -- The Garrison Once
  3:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 1, Count = 5, X = 1216, Y = -2915, Z = -63}
  4:  SummonMonsters  {TypeIndexInMapStats = 3, Level = 3, Count = 5, X = 874, Y = -3342, Z = -61}
  5:  SummonMonsters  {TypeIndexInMapStats = 3, Level = 2, Count = 7, X = -425, Y = -904, Z = 1}
  6:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 5, X = 2180, Y = 757, Z = -63}
  7:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 5, X = -3563, Y = 10369, Z = -2431}
  8:  Cmp  {"QBits", Value = 508,   jump = 10}         -- Warrior
  9:  Exit  {}

  10: SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 4, X = 435, Y = -2645, Z = -54}
  11: SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 3, X = 2170, Y = 750, Z = -63}
  12: SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 5, X = -2466, Y = -725, Z = 1}
  13: SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 9, X = 0, Y = 1186, Z = -1279}
  14: SummonMonsters  {TypeIndexInMapStats = 3, Level = 2, Count = 7, X = -967, Y = 4154, Z = -1279}
  15: SummonMonsters  {TypeIndexInMapStats = 3, Level = 3, Count = 6, X = -2614, Y = 6192, Z = -2431}
  16: SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 6, X = -2043, Y = 8822, Z = -2431}
  17: SummonMonsters  {TypeIndexInMapStats = 3, Level = 3, Count = 5, X = 16, Y = 11346, Z = -2431}
  18: Cmp  {"QBits", Value = 507,   jump = 20}         -- Death Wish
  19: Exit  {}

  20: SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 7, X = 893, Y = -2334, Z = -35}
  21: SummonMonsters  {TypeIndexInMapStats = 3, Level = 3, Count = 7, X = -2930, Y = -1352, Z = 1}
  22: SummonMonsters  {TypeIndexInMapStats = 3, Level = 3, Count = 7, X = -984, Y = 1196, Z = -1279}
  23: SummonMonsters  {TypeIndexInMapStats = 3, Level = 3, Count = 7, X = -1315, Y = 4099, Z = -1279}
  24: SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 7, X = -2619, Y = 5708, Z = -2431}
  25: SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 7, X = -281, Y = 11157, Z = -2431}
  26: Exit  {}
end
