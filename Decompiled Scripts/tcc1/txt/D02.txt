str[0] = " "
str[1] = "Door"
str[2] = "Exit"
str[3] = "Chest"
str[4] = "Guano Rock"
str[5] = "You've Rescued a child!  How patriarchal of you"
str[6] = "This Rock is covered in bat guano"
str[7] = "You harvest the guano and put it in your pouch"
str[8] = "You pick up some guano, but have nowhere to put it."
str[9] = "Cage"
str[10] = "You open the cage and remove the bones."
str[11] = "Teleporter"
str[12] = "Underwald"
str[13] = ""


event 1
      Hint = str[1]  -- "Door"
      MazeInfo = str[12]  -- "Underwald"
  0:  SetDoorState  {Id = 1, State = 1}
end

event 2
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 2, State = 1}
end

event 3
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 3, State = 1}
end

event 4
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 4, State = 1}
  1:  SetDoorState  {Id = 5, State = 1}
end

event 5
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 5, State = 1}
  1:  SetDoorState  {Id = 4, State = 1}
end

event 6
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 6, State = 1}
end

event 7
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 7, State = 1}
end

event 8
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 8, State = 1}
end

event 9
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 9, State = 1}
end

event 11
      Hint = str[11]  -- "Teleporter"
  0:  MoveToMap  {X = 247, Y = 2331, Z = -740, Direction = 1088, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 12
  0:  OpenChest  {Id = 1}
end

event 14
  0:  Cmp  {"QBits", Value = 443,   jump = 7}         -- Underwald Special Once
  1:  Set  {"QBits", Value = 443}         -- Underwald Special Once
  2:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 2, X = 3350, Y = 6328, Z = -1234}
  3:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 1, Count = 3, X = 3305, Y = 6481, Z = -1227}
  4:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 2, X = 2942, Y = 6113, Z = -1211}
  5:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 1, Count = 3, X = 3282, Y = 5171, Z = -1080}
  6:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 2, X = 2587, Y = 4985, Z = -1080}
  7:  Exit  {}
end

event 16
      Hint = str[9]  -- "Cage"
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  Set  {"MapVar0", Value = 1}
  2:  GiveItem  {Strength = 1, Type = const.ItemType.Ring_, Id = 0}
  3:  Exit  {}
end

event 17
      Hint = str[9]  -- "Cage"
  0:  Cmp  {"MapVar1", Value = 1,   jump = 3}
  1:  Set  {"MapVar1", Value = 1}
  2:  GiveItem  {Strength = 1, Type = const.ItemType.Amulet_, Id = 0}
  3:  Exit  {}
end

event 18
      Hint = str[9]  -- "Cage"
  0:  Cmp  {"MapVar2", Value = 1,   jump = 3}
  1:  Set  {"MapVar2", Value = 1}
  2:  GiveItem  {Strength = 1, Type = const.ItemType.Weapon_, Id = 0}
  3:  Exit  {}
end

event 19
      Hint = str[9]  -- "Cage"
  0:  Cmp  {"MapVar3", Value = 1,   jump = 3}
  1:  Set  {"MapVar3", Value = 1}
  2:  GiveItem  {Strength = 2, Type = const.ItemType.Ring_, Id = 0}
  3:  Exit  {}
end

event 20
      Hint = str[9]  -- "Cage"
  0:  Cmp  {"MapVar4", Value = 1,   jump = 3}
  1:  Set  {"MapVar4", Value = 1}
  2:  GiveItem  {Strength = 2, Type = const.ItemType.Amulet_, Id = 0}
  3:  Exit  {}
end

event 21
      Hint = str[9]  -- "Cage"
  0:  Cmp  {"MapVar5", Value = 1,   jump = 3}
  1:  Set  {"MapVar5", Value = 1}
  2:  GiveItem  {Strength = 2, Type = const.ItemType.Weapon_, Id = 0}
  3:  Exit  {}
end

event 22
      Hint = str[9]  -- "Cage"
  0:  Cmp  {"MapVar6", Value = 1,   jump = 4}
  1:  Set  {"MapVar6", Value = 1}
  2:  GiveItem  {Strength = 2, Type = const.ItemType.Scroll_, Id = 0}
  3:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 8, FromX = 7808, FromY = 8960, FromZ = -1768, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  4:  Exit  {}
end

event 23
      Hint = str[9]  -- "Cage"
  0:  Cmp  {"MapVar7", Value = 1,   jump = 4}
  1:  Set  {"MapVar7", Value = 1}
  2:  GiveItem  {Strength = 2, Type = const.ItemType.Scroll_, Id = 0}
  3:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 8, FromX = 7808, FromY = 8960, FromZ = -1768, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  4:  Exit  {}
end

event 25
      Hint = str[1]  -- "Door"
  0:  StatusText  {Str = 5}         -- "You've Rescued a child!  How patriarchal of you"
end

event 26
      Hint = str[3]  -- "Chest"
  0:  Cmp  {"MapVar1", Value = 1,   jump = 3}
  1:  Cmp  {"QBits", Value = 34,   jump = 6}         -- 34 D02, given when temple of Baa relic is found
  2:  Set  {"MapVar1", Value = 1}
  3:  OpenChest  {Id = 2}
  4:  Set  {"QBits", Value = 34}         -- 34 D02, given when temple of Baa relic is found
  5:  Exit  {}

  6:  OpenChest  {Id = 4}
end

event 27
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 3}
end

event 28
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 5}
end

event 29
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 6}
end

event 30
  0:  OnTimer  {IntervalInHalfMinutes = 10}
  1:  CastSpell  {Spell = 15, Mastery = const.Novice, Skill = 10, FromX = 11040, FromY = 6384, FromZ = -176, ToX = 11040, ToY = 6384, ToZ = 0}         -- "Sparks"
  2:  CastSpell  {Spell = 15, Mastery = const.Novice, Skill = 10, FromX = 11040, FromY = 6384, FromZ = -192, ToX = 15033, ToY = 5785, ToZ = -256}         -- "Sparks"
  3:  CastSpell  {Spell = 15, Mastery = const.Novice, Skill = 10, FromX = 11040, FromY = 6384, FromZ = -200, ToX = 11188, ToY = 4279, ToZ = -256}         -- "Sparks"
  4:  CastSpell  {Spell = 15, Mastery = const.Novice, Skill = 10, FromX = 11040, FromY = 6485, FromZ = -369, ToX = 7967, ToY = 6446, ToZ = -256}         -- "Sparks"
end

event 31
      Hint = str[11]  -- "Teleporter"
  0:  MoveToMap  {X = 16519, Y = -18589, Z = 753, Direction = 1024, LookAngle = 50, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 35
  0:  Cmp  {"MapVar29", Value = 1,   jump = 3}
  1:  Set  {"MapVar29", Value = 1}
  2:  GiveItem  {Strength = 4, Type = const.ItemType.Ring_, Id = 0}
  3:  Exit  {}
end

event 50
      Hint = str[2]  -- "Exit"
  0:  MoveToMap  {X = 15585, Y = 11125, Z = 97, Direction = 512, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 6, Name = "OutE3.odm"}
end

event 60
  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 444,   jump = 6}         -- Underwald Once
  2:  Set  {"QBits", Value = 444}         -- Underwald Once
  3:  SummonMonsters  {TypeIndexInMapStats = 2, Level = 2, Count = 1, X = 11177, Y = 5872, Z = -432}
  4:  SummonMonsters  {TypeIndexInMapStats = 2, Level = 2, Count = 1, X = 729, Y = 2612, Z = -740}
  5:  Cmp  {"QBits", Value = 508,   jump = 7}         -- Warrior
  6:  Exit  {}

  7:  SummonMonsters  {TypeIndexInMapStats = 2, Level = 2, Count = 2, X = 11372, Y = 6295, Z = -432}
  8:  SummonMonsters  {TypeIndexInMapStats = 2, Level = 2, Count = 2, X = -10644, Y = -5944, Z = -1234}
  9:  Cmp  {"QBits", Value = 507,   jump = 11}         -- Death Wish
  10: Exit  {}

  11: SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 5, X = 10473, Y = 6854, Z = -432}
  12: SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 7, X = 183, Y = 2476, Z = -432}
  13: SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 7, X = -10640, Y = -5950, Z = -1234}
  14: Exit  {}
end
