if Game.Version == 8 then
	return
end

function events.Tick()
	if vars and vars.PartySize then
		for i = vars.PartySize, 3 do
			local pl = Party[i]
			pl.Conditions[const.Condition.Stoned] = 1
			pl.Conditions[const.Condition.Weak] = 0
			pl.HP = -1
			pl.SP = -1
		end
	end
	if vars and ((Game.PatchOptions or {}).UILayout or "") ~= "" then
		Game.Dll.UILayoutSetVarInt('Game.Players', vars.PartySize or 4)
	end
end
